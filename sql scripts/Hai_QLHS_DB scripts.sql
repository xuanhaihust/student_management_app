USE [STUDENT_DB]
GO
/****** Object:  Table [dbo].[BTVN]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BTVN](
	[MaMH] [nchar](20) NOT NULL,
	[MaHS] [nchar](20) NOT NULL,
	[NamHoc] [int] NULL,
	[BaiTap1] [nvarchar](50) NULL,
	[NgayNop_BT1] [date] NULL,
	[Dien_BT1] [numeric](4, 2) NULL,
	[BaiTap2] [nvarchar](50) NULL,
	[NgayNop_BT2] [date] NULL,
	[Dien_BT2] [numeric](4, 2) NULL,
 CONSTRAINT [BTVN_pk] PRIMARY KEY CLUSTERED 
(
	[MaHS] ASC,
	[MaMH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DiemDanh]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiemDanh](
	[MaGV] [nchar](20) NOT NULL,
	[MaHS] [nchar](20) NOT NULL,
	[DiHoc] [int] NULL,
	[NghiCoPhep] [int] NULL,
	[NghiKhongPhep] [int] NULL,
 CONSTRAINT [DiemDanh_pk] PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC,
	[MaHS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DiemHoc]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiemHoc](
	[MaMH] [nchar](20) NOT NULL,
	[MaHS] [nchar](20) NOT NULL,
	[HK1_HS1] [numeric](4, 2) NULL,
	[HK1_HS2] [numeric](4, 2) NULL,
	[HK1_ThiHK] [numeric](4, 2) NULL,
	[HK1_TK] [numeric](4, 2) NULL,
	[HK2_HS1] [numeric](4, 2) NULL,
	[HK2_HS2] [numeric](4, 2) NULL,
	[HK2_ThiHK] [numeric](4, 2) NULL,
	[HK2_TK] [numeric](4, 2) NULL,
	[TongKet] [numeric](4, 2) NULL,
 CONSTRAINT [Diem_pk] PRIMARY KEY CLUSTERED 
(
	[MaHS] ASC,
	[MaMH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiangDay]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiangDay](
	[MaGV] [nchar](20) NOT NULL,
	[MaMH] [nchar](20) NOT NULL,
 CONSTRAINT [GD_pk] PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC,
	[MaMH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GiaoVien]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GiaoVien](
	[MaGV] [nchar](20) NOT NULL,
	[TenGV] [nvarchar](50) NOT NULL,
	[NgaySinh] [date] NULL,
	[GioiTinh] [nvarchar](10) NULL,
	[Email] [varchar](20) NULL,
 CONSTRAINT [MaGV_pk] PRIMARY KEY CLUSTERED 
(
	[MaGV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[HocSinh]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HocSinh](
	[MaHS] [nchar](20) NOT NULL,
	[TenHS] [nvarchar](50) NOT NULL,
	[NgaySinh] [date] NULL,
	[GioiTinh] [nvarchar](10) NULL,
	[PhuHuynh] [nvarchar](50) NULL,
	[DiaChi] [nvarchar](200) NULL,
	[SDT_PH] [nchar](10) NULL,
	[MaLop] [nchar](20) NOT NULL,
	[Email] [varchar](20) NULL,
 CONSTRAINT [MaHS_pk] PRIMARY KEY CLUSTERED 
(
	[MaHS] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LopHoc]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LopHoc](
	[MaLop] [nchar](20) NOT NULL,
	[GVCN] [nchar](20) NOT NULL,
	[PhongHoc] [nvarchar](50) NULL,
 CONSTRAINT [MaLop_pk] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MonHoc]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MonHoc](
	[MaMH] [nchar](20) NOT NULL,
	[TenMH] [nvarchar](50) NOT NULL,
	[SoTiet] [int] NULL,
 CONSTRAINT [MaMH_pk] PRIMARY KEY CLUSTERED 
(
	[MaMH] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[QuanLyLichDay]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QuanLyLichDay](
	[MaLop] [nchar](20) NOT NULL,
	[MaGV] [nchar](20) NOT NULL,
	[NamHoc] [int] NULL,
	[KyHoc] [int] NULL,
	[Monday] [nvarchar](20) NULL,
	[Tuesday] [nvarchar](20) NULL,
	[Wednesday] [nvarchar](20) NULL,
	[Thursday] [nvarchar](20) NULL,
	[Friday] [nvarchar](20) NULL,
	[MaMH] [nchar](20) NULL,
 CONSTRAINT [LichHoc_pk] PRIMARY KEY CLUSTERED 
(
	[MaLop] ASC,
	[MaGV] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 4/21/2020 11:05:17 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](20) NOT NULL,
	[email] [varchar](120) NOT NULL,
	[image_file] [varchar](20) NOT NULL,
	[password] [varchar](60) NOT NULL,
	[permission] [nchar](10) NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS1                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V06                 ', N'HS1                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H06                 ', N'HS10                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS10                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V07                 ', N'HS11                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V07                 ', N'HS12                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V07                 ', N'HS13                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V07                 ', N'HS14                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V07                 ', N'HS15                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H07                 ', N'HS16                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H07                 ', N'HS17                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H07                 ', N'HS18                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H07                 ', N'HS19                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS2                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V06                 ', N'HS2                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H07                 ', N'HS20                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS3                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V06                 ', N'HS3                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS4                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V06                 ', N'HS4                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS5                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'V06                 ', N'HS5                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H06                 ', N'HS6                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS6                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H06                 ', N'HS7                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS7                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H06                 ', N'HS8                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS8                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'H06                 ', N'HS9                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[BTVN] ([MaMH], [MaHS], [NamHoc], [BaiTap1], [NgayNop_BT1], [Dien_BT1], [BaiTap2], [NgayNop_BT2], [Dien_BT2]) VALUES (N'T06                 ', N'HS9                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS1                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS11                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS12                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS13                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS14                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS15                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS2                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS21                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS3                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS4                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV1                 ', N'HS5                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV2                 ', N'HS1                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV2                 ', N'HS2                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV2                 ', N'HS21                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV2                 ', N'HS3                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV2                 ', N'HS4                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV2                 ', N'HS5                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV3                 ', N'HS10                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV3                 ', N'HS6                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV3                 ', N'HS7                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV3                 ', N'HS8                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV3                 ', N'HS9                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS10                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS16                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS17                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS18                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS19                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS20                ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS6                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS7                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS8                 ', NULL, NULL, NULL)
INSERT [dbo].[DiemDanh] ([MaGV], [MaHS], [DiHoc], [NghiCoPhep], [NghiKhongPhep]) VALUES (N'GV5                 ', N'HS9                 ', NULL, NULL, NULL)
GO
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS1                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V06                 ', N'HS1                 ', CAST(4.00 AS Numeric(4, 2)), CAST(5.00 AS Numeric(4, 2)), CAST(6.00 AS Numeric(4, 2)), CAST(5.33 AS Numeric(4, 2)), CAST(1.00 AS Numeric(4, 2)), CAST(5.00 AS Numeric(4, 2)), CAST(6.00 AS Numeric(4, 2)), CAST(4.83 AS Numeric(4, 2)), CAST(5.08 AS Numeric(4, 2)))
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H06                 ', N'HS10                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS10                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V07                 ', N'HS11                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V07                 ', N'HS12                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V07                 ', N'HS13                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V07                 ', N'HS14                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V07                 ', N'HS15                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H07                 ', N'HS16                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H07                 ', N'HS17                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H07                 ', N'HS18                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H07                 ', N'HS19                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS2                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V06                 ', N'HS2                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H07                 ', N'HS20                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS21                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V06                 ', N'HS21                ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS3                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V06                 ', N'HS3                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS4                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V06                 ', N'HS4                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS5                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'V06                 ', N'HS5                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H06                 ', N'HS6                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS6                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H06                 ', N'HS7                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS7                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H06                 ', N'HS8                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS8                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'H06                 ', N'HS9                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
INSERT [dbo].[DiemHoc] ([MaMH], [MaHS], [HK1_HS1], [HK1_HS2], [HK1_ThiHK], [HK1_TK], [HK2_HS1], [HK2_HS2], [HK2_ThiHK], [HK2_TK], [TongKet]) VALUES (N'T06                 ', N'HS9                 ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV1                 ', N'V06                 ')
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV1                 ', N'V07                 ')
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV2                 ', N'T06                 ')
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV3                 ', N'T06                 ')
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV4                 ', N'T07                 ')
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV5                 ', N'H06                 ')
INSERT [dbo].[GiangDay] ([MaGV], [MaMH]) VALUES (N'GV5                 ', N'H07                 ')
GO
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV1                 ', N'Nguy?n Phuong Mai', CAST(N'1991-08-20' AS Date), N'N?', N'Mai.np@gmail.com')
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV2                 ', N'Nguy?n Th? Hà', CAST(N'1990-08-10' AS Date), N'N?', N'Ha.nt@gmail.com')
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV3                 ', N'Nguy?n H?i Van', CAST(N'1988-04-03' AS Date), N'Nam', N'Van.nh@gmail.com')
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV4                 ', N'Nguy?n Van Anh', CAST(N'1989-08-03' AS Date), N'Nam', N'Anh.nv@gmail.com')
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV5                 ', N'Nguy?n Th? Hu?', CAST(N'1992-05-10' AS Date), N'N?', N'Hue.nt@gmail.com')
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV6                 ', N'Nguyễn Thị A', CAST(N'1992-02-03' AS Date), N'Nữ', N'a.nt@gmail.com')
INSERT [dbo].[GiaoVien] ([MaGV], [TenGV], [NgaySinh], [GioiTinh], [Email]) VALUES (N'GV7                 ', N'Nguyễn Thị B', CAST(N'1992-02-03' AS Date), N'Nữ', N'b.nt@gmail.com')
GO
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS1                 ', N'Nguyễn Văn Toán', CAST(N'2006-02-03' AS Date), N'Nam', N'Nguyễn Văn Long', N'', N'42422     ', N'6A1                 ', N'Toan.nv@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS10                ', N'Nguyễn Thái Tình', CAST(N'2006-03-02' AS Date), N'Nam', N'Nguyễn Minh Anh', NULL, NULL, N'6A2                 ', N'Tinh.nt@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS11                ', N'Đặng Quỳnh Ngân', CAST(N'2005-04-05' AS Date), N'Nữ', N'Đặng Thị Tuyết', N'', N'          ', N'7A1                 ', N'Ngan.dq@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS12                ', N'Vũ Ngọc Linh', CAST(N'2005-07-04' AS Date), N'Nữ ', N'Vũ Mai Trung', NULL, NULL, N'7A1                 ', N'Linh.vn@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS13                ', N'Lê Phước Thuận', CAST(N'2005-08-12' AS Date), N'Nam', N'Lê Văn Nam', NULL, NULL, N'7A1                 ', N'Thuan.lp@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS14                ', N'Nguyễn Thị Quỳnh', CAST(N'2005-09-12' AS Date), N'Nữ', N'Nguyễn Thị Tươi', NULL, NULL, N'7A1                 ', N'Quynh.nt@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS15                ', N'Trần Phương Thảo', CAST(N'2005-03-17' AS Date), N'Nữ', N'Trần Văn Binh', NULL, NULL, N'7A1                 ', N'Thao.tp@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS16                ', N'Hoàng Văn Tuấn', CAST(N'2005-06-07' AS Date), N'Nam', N'Hoàng Văn Hưng', N'', N'          ', N'7A2                 ', N'Tuan.hv@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS17                ', N'Lâm Quỳnh Như', CAST(N'2005-09-10' AS Date), N'Nữ', N'Lâm Quang Minh', NULL, NULL, N'7A2                 ', N'Nhu.lq@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS18                ', N'Nguyễn Quốc Toàn', CAST(N'2005-10-23' AS Date), N'Nam', N'Nguyễn Văn Bá', NULL, NULL, N'7A2                 ', N'Toan.nq@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS19                ', N'Phạm Hồng Thắng', CAST(N'2005-06-02' AS Date), N'Nam', N'Phạm Hải Yến', NULL, NULL, N'7A2                 ', N'Thang.ph@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS2                 ', N'Nguyễn Vân Anh', CAST(N'2006-08-09' AS Date), N'Nữ', N'Nguyễn Thị Vân', NULL, NULL, N'6A1                 ', N'Anh.nv@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS20                ', N'Nguyễn Mai Anh', CAST(N'2005-02-23' AS Date), N'Nữ ', N'Nguyễn Văn Tuyển', NULL, NULL, N'7A2                 ', N'Anh.nm@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS21                ', N'Ph?m Th? Linh', CAST(N'2006-03-04' AS Date), N'N?', NULL, NULL, NULL, N'6A1                 ', N'Linh.pt@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS3                 ', N'Lý Gia Bảo', CAST(N'2006-12-03' AS Date), N'Nam', N'Lý Nhật Hạ', NULL, NULL, N'6A1                 ', N'Bao.ng@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS4                 ', N'Nguyễn Đăng Khôi', CAST(N'2006-11-22' AS Date), N'Nam', N'Nguyễn Đăng Mạnh', NULL, NULL, N'6A1                 ', N'Khoi.nd@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS5                 ', N'Đinh Xuân Mai', CAST(N'2006-11-11' AS Date), N'Nữ', N'Đinh Hồng Phúc', NULL, NULL, N'6A1                 ', N'Mai.dx@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS6                 ', N'Võ Khánh Nhi', CAST(N'2006-05-11' AS Date), N'Nữ', N'Nguyễn Phương Thảo', NULL, NULL, N'6A2                 ', N'Nhi.vk@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS7                 ', N'Nguyễn Hoàng Quân', CAST(N'2006-08-09' AS Date), N'Nam', N'Nguyễn Văn Lâm', NULL, NULL, N'6A2                 ', N'Quan.nh@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS8                 ', N'Nguyễn Thanh Thảo', CAST(N'2006-03-04' AS Date), N'Nữ', N'Nguyễn Huy Tiến', NULL, NULL, N'6A2                 ', N'Thao.nt@gmail.com')
INSERT [dbo].[HocSinh] ([MaHS], [TenHS], [NgaySinh], [GioiTinh], [PhuHuynh], [DiaChi], [SDT_PH], [MaLop], [Email]) VALUES (N'HS9                 ', N'Trần Tấn Lợi', CAST(N'2006-04-05' AS Date), N'Nam', N'Trần Minh Sang', NULL, NULL, N'6A2                 ', N'Loi.tt@gmail.com')
GO
INSERT [dbo].[LopHoc] ([MaLop], [GVCN], [PhongHoc]) VALUES (N'6A1                 ', N'GV1                 ', N'A102')
INSERT [dbo].[LopHoc] ([MaLop], [GVCN], [PhongHoc]) VALUES (N'6A2                 ', N'GV2                 ', N'A103')
INSERT [dbo].[LopHoc] ([MaLop], [GVCN], [PhongHoc]) VALUES (N'6A3                 ', N'GV5                 ', N'P102')
INSERT [dbo].[LopHoc] ([MaLop], [GVCN], [PhongHoc]) VALUES (N'7A1                 ', N'GV3                 ', N'A201')
INSERT [dbo].[LopHoc] ([MaLop], [GVCN], [PhongHoc]) VALUES (N'7A2                 ', N'GV4                 ', N'A202')
GO
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [SoTiet]) VALUES (N'H06                 ', N'Hóa l?p 6', 45)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [SoTiet]) VALUES (N'H07                 ', N'Hóa l?p 7', 45)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [SoTiet]) VALUES (N'T06                 ', N'Toán l?p 6', 45)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [SoTiet]) VALUES (N'T07                 ', N'Toán l?p 7', 45)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [SoTiet]) VALUES (N'V06                 ', N'Van l?p 6', 45)
INSERT [dbo].[MonHoc] ([MaMH], [TenMH], [SoTiet]) VALUES (N'V07                 ', N'Van l?p 7', 45)
GO
INSERT [dbo].[QuanLyLichDay] ([MaLop], [MaGV], [NamHoc], [KyHoc], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [MaMH]) VALUES (N'6A1                 ', N'GV1                 ', 2020, 2, N'Tiết 1', NULL, N'Tiết 2', NULL, NULL, N'V06                 ')
INSERT [dbo].[QuanLyLichDay] ([MaLop], [MaGV], [NamHoc], [KyHoc], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [MaMH]) VALUES (N'6A1                 ', N'GV2                 ', 2020, 2, NULL, N'Tiết 1', NULL, N'Tiết 2', NULL, N'T06                 ')
INSERT [dbo].[QuanLyLichDay] ([MaLop], [MaGV], [NamHoc], [KyHoc], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [MaMH]) VALUES (N'6A2                 ', N'GV3                 ', 2020, 2, N'Tiết 2', NULL, NULL, NULL, N'Tiết 1', N'T06                 ')
INSERT [dbo].[QuanLyLichDay] ([MaLop], [MaGV], [NamHoc], [KyHoc], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [MaMH]) VALUES (N'6A2                 ', N'GV5                 ', 2020, 2, NULL, N'Tiết 2', NULL, N'Tiết 1', NULL, N'H06                 ')
INSERT [dbo].[QuanLyLichDay] ([MaLop], [MaGV], [NamHoc], [KyHoc], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [MaMH]) VALUES (N'7A1                 ', N'GV1                 ', 2020, 2, N'Tiết 2', NULL, N'Tiết 1', NULL, NULL, N'V07                 ')
INSERT [dbo].[QuanLyLichDay] ([MaLop], [MaGV], [NamHoc], [KyHoc], [Monday], [Tuesday], [Wednesday], [Thursday], [Friday], [MaMH]) VALUES (N'7A2                 ', N'GV5                 ', 2020, 2, N'Tiết 1', NULL, N'TIết 2', NULL, NULL, N'H07                 ')
GO
SET IDENTITY_INSERT [dbo].[TaiKhoan] ON 

INSERT [dbo].[TaiKhoan] ([id], [username], [email], [image_file], [password], [permission]) VALUES (4, N'phuongthao', N'Phuonthao@gmail.com', N'default.jpg', N'd9e43acc20b4c1bf68d2ccff8af9a0f8', N'2         ')
INSERT [dbo].[TaiKhoan] ([id], [username], [email], [image_file], [password], [permission]) VALUES (6, N'xuanhaihust', N'xuanhaihust58@gmail.com', N'abc.jpg', N'81332434eaa3a1e69fd9f6d2c40614d6', N'1         ')
SET IDENTITY_INSERT [dbo].[TaiKhoan] OFF
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__user__AB6E616412402770]    Script Date: 4/21/2020 11:05:17 AM ******/
ALTER TABLE [dbo].[TaiKhoan] ADD UNIQUE NONCLUSTERED 
(
	[email] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__user__F3DBC572D1DF5300]    Script Date: 4/21/2020 11:05:17 AM ******/
ALTER TABLE [dbo].[TaiKhoan] ADD UNIQUE NONCLUSTERED 
(
	[username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[BTVN]  WITH CHECK ADD  CONSTRAINT [BTVN_MaHS_fk] FOREIGN KEY([MaHS])
REFERENCES [dbo].[HocSinh] ([MaHS])
GO
ALTER TABLE [dbo].[BTVN] CHECK CONSTRAINT [BTVN_MaHS_fk]
GO
ALTER TABLE [dbo].[BTVN]  WITH CHECK ADD  CONSTRAINT [BTVN_MaMH_fk] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonHoc] ([MaMH])
GO
ALTER TABLE [dbo].[BTVN] CHECK CONSTRAINT [BTVN_MaMH_fk]
GO
ALTER TABLE [dbo].[DiemDanh]  WITH CHECK ADD  CONSTRAINT [DD_MaGV_fk] FOREIGN KEY([MaGV])
REFERENCES [dbo].[GiaoVien] ([MaGV])
GO
ALTER TABLE [dbo].[DiemDanh] CHECK CONSTRAINT [DD_MaGV_fk]
GO
ALTER TABLE [dbo].[DiemDanh]  WITH CHECK ADD  CONSTRAINT [DD_MaHS_fk] FOREIGN KEY([MaHS])
REFERENCES [dbo].[HocSinh] ([MaHS])
GO
ALTER TABLE [dbo].[DiemDanh] CHECK CONSTRAINT [DD_MaHS_fk]
GO
ALTER TABLE [dbo].[DiemHoc]  WITH CHECK ADD  CONSTRAINT [BT_MaHS_fk] FOREIGN KEY([MaHS])
REFERENCES [dbo].[HocSinh] ([MaHS])
GO
ALTER TABLE [dbo].[DiemHoc] CHECK CONSTRAINT [BT_MaHS_fk]
GO
ALTER TABLE [dbo].[DiemHoc]  WITH CHECK ADD  CONSTRAINT [DH_MaMH_fk] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonHoc] ([MaMH])
GO
ALTER TABLE [dbo].[DiemHoc] CHECK CONSTRAINT [DH_MaMH_fk]
GO
ALTER TABLE [dbo].[GiangDay]  WITH CHECK ADD  CONSTRAINT [GD_MaGV_fk] FOREIGN KEY([MaGV])
REFERENCES [dbo].[GiaoVien] ([MaGV])
GO
ALTER TABLE [dbo].[GiangDay] CHECK CONSTRAINT [GD_MaGV_fk]
GO
ALTER TABLE [dbo].[GiangDay]  WITH CHECK ADD  CONSTRAINT [GD_MaMH_fk] FOREIGN KEY([MaMH])
REFERENCES [dbo].[MonHoc] ([MaMH])
GO
ALTER TABLE [dbo].[GiangDay] CHECK CONSTRAINT [GD_MaMH_fk]
GO
ALTER TABLE [dbo].[HocSinh]  WITH CHECK ADD  CONSTRAINT [HS_MaLop_fk] FOREIGN KEY([MaLop])
REFERENCES [dbo].[LopHoc] ([MaLop])
GO
ALTER TABLE [dbo].[HocSinh] CHECK CONSTRAINT [HS_MaLop_fk]
GO
ALTER TABLE [dbo].[QuanLyLichDay]  WITH CHECK ADD  CONSTRAINT [QL_MaGV_fk] FOREIGN KEY([MaGV])
REFERENCES [dbo].[GiaoVien] ([MaGV])
GO
ALTER TABLE [dbo].[QuanLyLichDay] CHECK CONSTRAINT [QL_MaGV_fk]
GO
ALTER TABLE [dbo].[QuanLyLichDay]  WITH CHECK ADD  CONSTRAINT [QL_MaLop_fk] FOREIGN KEY([MaLop])
REFERENCES [dbo].[LopHoc] ([MaLop])
GO
ALTER TABLE [dbo].[QuanLyLichDay] CHECK CONSTRAINT [QL_MaLop_fk]
GO

ALTER DATABASE [STUDENT_DB] SET  READ_WRITE 
GO
