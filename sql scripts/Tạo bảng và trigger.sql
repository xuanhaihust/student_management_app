﻿Create database CSDL_BTN
use CSDL_BTN
go

---- tạo các bảng 
Create table LopHoc (
	MaLop NCHAR(20) NOT NULL,
	TenLop NVARCHAR(50) NOT NULL,
	GVCN NCHAR(20) Not null,
	PhongHoc NVARCHAR(50),
	Constraint MaLop_pk primary key (MaLop)
)
------------------------------------
Create table HocSinh (
	MaHS NCHAR(20) NOT NULL,
	TenHS NVARCHAR(50) NOT NULL,
	NgaySinh Date,
	GioiTinh NVARCHAR(10),
	PhuHuynh NVARCHAR(50),
	DiaChi NVARCHAR(200),
	SDT_PH nchar(10),
	MaLop NCHAR(20) NOT NULL,
	Email VARCHAR(20),
	Constraint MaHS_pk primary key (MaHS),
	CONSTRAINT HS_MaLop_fk FOREIGN KEY (MaLop) REFERENCES LopHoc (MaLop),

)

Create table MonHoc (
	MaMH NCHAR(20) NOT NULL,
	TenMH NVARCHAR(50) NOT NULL,
	SoTiet INT,
	constraint MaMH_pk primary key (MaMH)
)

Create table GiaoVien(
	MaGV NCHAR(20) NOT NULL,
	TenGV NVARCHAR(50) NOT NULL,
	NgaySinh Date,
	GioiTinh NVARCHAR(10),
	Email VARCHAR(20)
	constraint MaGV_pk primary key (MaGV)
)

Create table DiemDanh (
	MaGV NChar(20) not null,
	MaHS NCHAR(20) NOT NULL,
	DiHoc int,
	NghiCoPhep int,
	NghiKhongPhep int,
	Constraint DiemDanh_pk primary key (MaGV, MaHS),
	CONSTRAINT DD_MaGV_fk FOREIGN KEY (MaGV) REFERENCES GiaoVien (MaGV),
	CONSTRAINT DD_MaHS_fk FOREIGN KEY (MaHS) REFERENCES HocSinh (MaHS)
)
go
Create table QuanLyLichDay (
	MaLop nchar(20) not null,
	MaGV nchar(20) not null,
	NamHoc int,
	KyHoc int,
	Monday nvarchar(20),
	Tuesday nvarchar(20),
	Wednesday nvarchar(20),
	Thursday nvarchar(20),
	Friday nvarchar(20),
	Constraint LichHoc_pk primary key (MaLop,MaGV),
	CONSTRAINT QL_MaLop_fk FOREIGN KEY (MaLop) REFERENCES LopHoc (MaLop),
	CONSTRAINT QL_MaGV_fk FOREIGN KEY (MaGV) REFERENCES GiaoVien (MaGV),

)

drop table DiemHoc
Create table DiemHoc (
	MaMH nchar(20) Not null,
	MaHS nchar(20) not null,
	HK1_HS1 numeric(4,2) ,
	HK1_HS2 numeric(4,2),
	HK1_ThiHK numeric(4,2),
	HK1_TK numeric(4,2),
	HK2_HS1 numeric(4,2),
	HK2_HS2 numeric(4,2),
	HK2_ThiHK numeric(4,2),
	HK2_TK numeric(4,2),

	Constraint Diem_pk primary key (MaHS,MaMH),
	CONSTRAINT DH_MaMH_fk FOREIGN KEY (MaMH) REFERENCES MonHoc (MaMH),
	CONSTRAINT BT_MaHS_fk FOREIGN KEY (MaHS) REFERENCES HocSinh (MaHS)

)

Create table BTVN (
	MaMH nchar(20) not null,
	MaHS nchar(20) not null,
	NamHoc int,
	BaiTap1 nvarchar(50),
	NgayNop_BT1 date,
	Dien_BT1 numeric(4,2),
	BaiTap2 nvarchar(50),
	NgayNop_BT2 date,
	Dien_BT2 numeric(4,2),
	Constraint BTVN_pk primary key (MaHS,MaMH),
	CONSTRAINT BTVN_MaMH_fk FOREIGN KEY (MaMH) REFERENCES MonHoc (MaMH),
	CONSTRAINT BTVN_MaHS_fk FOREIGN KEY (MaHS) REFERENCES HocSinh (MaHS)
)


Create table GiangDay (
	MaGV nchar(20) not null,
	MaMH nchar(20) not null,
	Constraint GD_pk primary key (MaGV,MaMH),
)

----- tạo giàng buộc khóa ngoài
Alter table GiangDay 
	add CONSTRAINT GD_MaMH_fk FOREIGN KEY (MaMH) REFERENCES MonHoc (MaMH)

go
Alter table GiangDay add
CONSTRAINT GD_MaGV_fk FOREIGN KEY (MaGV) REFERENCES GiaoVien (MaGV)

---- tạo thêm cột vào bảng
Alter table QuanLyLichDay add MaMH nchar(20)
go 

Alter table DiemHoc add TongKet numeric(4,2)
go

-- Tạo ràng buộc check cho điểm số ở bảng điểm học
Alter table DiemHoc add constraint check_d1 check (HK1_HS1 >= 0 and HK1_HS1 <= 10)
Alter table DiemHoc add constraint check_d2 check (HK1_HS2 >= 0 and HK1_HS2 <= 10)
Alter table DiemHoc add constraint check_d3 check (HK1_ThiHK >= 0 and HK1_ThiHK <= 10)
Alter table DiemHoc add constraint check_d4 check (HK1_TK >= 0 and HK1_TK <= 10)

Alter table DiemHoc add constraint check_d5 check (HK2_HS1 >= 0 and HK2_HS1 <= 10)
Alter table DiemHoc add constraint check_d6 check (HK2_HS2 >= 0 and HK2_HS2 <= 10)
Alter table DiemHoc add constraint check_d7 check (HK2_ThiHK >= 0 and HK2_ThiHK <= 10)
Alter table DiemHoc add constraint check_d8 check (HK2_TK >= 0 and HK2_TK <= 10)

Alter table DiemHoc add constraint check_d9 check (TongKet >= 0 and TongKet <= 10)
go
-- tạo trigger để tự động insert giá trị
-- insert giá trị vào trường mã GV và mã Học sinh vào bảng điểm, sau khi đã nhập dữ liệu vào bảng học sinh, lớp học
---môn học, và quản lý lịch học

alter trigger Trigger_DD 
on HocSinh
after insert 
as begin
	insert into DiemDanh(MaGV, MaHS)
	select MaGV,MaHS
	from inserted, QuanLyLichDay where inserted.MaLop = QuanLyLichDay.MaLop
end

go

create trigger Trigger_DD2
on QuanLyLichDay
after insert 
as begin
	insert into DiemDanh(MaGV, MaHS)
	select MaGV,MaHS
	from inserted, HocSinh where inserted.MaLop = HocSinh.MaLop
end
go

-- insert giá trị vào trường mã môn học, và mã học sinh vào bảng BTVN và DiemHoc

create trigger Trigger_BTVN
on HocSinh
after insert 
as begin
	insert into BTVN(MaMH, MaHS)
	select MaMH,MaHS
	from inserted, QuanLyLichDay where inserted.MaLop = QuanLyLichDay.MaLop
end
go

create trigger Trigger_BTVN2
on QuanLyLichDay
after insert 
as begin
	insert into BTVN(MaMH, MaHS)
	select MaMH,MaHS
	from inserted, HocSinh where inserted.MaLop = HocSinh.MaLop
end

go
--- insert giá trị vào trường mã môn học, mã học sinh vào bảng DiemHoc


create trigger Trigger_DH
on HocSinh
after insert 
as begin
	insert into DiemHoc(MaMH, MaHS)
	select MaMH,MaHS
	from inserted, QuanLyLichDay where inserted.MaLop = QuanLyLichDay.MaLop
end
go

create trigger Trigger_DH2
on QuanLyLichDay
after insert 
as begin
	insert into DiemHoc(MaMH, MaHS)
	select MaMH,MaHS
	from inserted, HocSinh where inserted.MaLop = HocSinh.MaLop
end
go
---- Tao trigger cập nhật điểm trung bình trong bảng điểm

create trigger update_TB1
on DiemHoc
after update as
begin
	
	update DiemHoc
	set DiemHoc.HK1_TK = (i.HK1_HS1 +2*i.HK1_HS2 + 3 * i.HK1_ThiHK)/6,
	DiemHoc.HK2_TK = (i.HK2_HS1 +2*i.HK2_HS2 + 3 * i.HK2_ThiHK)/6
	from DiemHoc d
	inner join inserted i
	on d.MaMH = i.MaMH and d.MaHS = i.MaHS

	update DiemHoc
	set DiemHoc.TongKet = (d.HK1_TK + d.HK2_TK)/2
	from DiemHoc d
	inner join inserted i
	on d.MaMH = i.MaMH and d.MaHS = i.MaHS

end