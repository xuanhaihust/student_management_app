# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DiemDanh.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
import sys
import time
import os
from PyQt5 import QtCore, QtGui, QtWidgets

DRIVER = 'SQL SERVER'
SERVER_NAME = '172.16.71.130'
DB_NAME = 'CSDL_BTN'
UID = 'sa'
PWD = '@xuanhai'

def createconnection():
    conString = f'DRIVER={DRIVER};'\
                f'SERVER={SERVER_NAME};'\
                f'DATABASE={DB_NAME};'\
                f'UID={UID};'\
                f'PWD={PWD}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

class SearchDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(SearchDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Search")

        self.setWindowTitle("Search class")
        self.setFixedWidth(300)
        self.setFixedHeight(300)
        self.QBtn.clicked.connect(self.searchclass)
        layout = QVBoxLayout()

        self.searchMaGVinput = QLineEdit()
        self.searchMaGVinput.setPlaceholderText("Ma Giáo Viên")

        self.searchNaminput = QLineEdit()
        self.searchNaminput.setPlaceholderText("Năm Học")

        self.searchKyHocinput = QLineEdit()
        self.searchKyHocinput.setPlaceholderText("Kỳ Học")

        layout.addWidget(self.searchNaminput)
        layout.addWidget(self.searchMaGVinput)
        layout.addWidget(self.searchKyHocinput)
        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def searchclass(self):
        MaGV = ""
        MaGV = self.searchMaGVinput.text()
        Nam = int(self.searchNaminput.text())
        KyHoc = int(self.searchKyHocinput.text())

        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
            qry.prepare("SELECT QuanLyLichDay.*, TenGV from QuanLyLichDay inner join GiaoVien on QuanLyLichDay.MaGV = GiaoVien.MaGV \
                         Where QuanLyLichDay.MaGV = ? and NamHoc = ? and KyHoc = ?")
                        
            qry.bindValue(0, MaGV)
            qry.bindValue(1, Nam)
            qry.bindValue(2, KyHoc)

            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
            if model.rowCount() > 0:
                QMessageBox.information(QMessageBox(), 'Successful', 'search successful')
                return model
            else:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không thể tìm thấy kết quả')
                return model
                
            db.close()


class Ui_GVWindowLD(object):

    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchclass()
        if model.rowCount() >0:
            self.tableWidget.setRowCount(0)
            Head = ['MaLop', 'MaGV', 'NamHoc', 'KyHoc', 'Monday', 'Tuesday', 'Wednesday','Thursday','Friday', 'MaMH']

            TenGV = model.record(0).value('TenGV')

            self.label_4.setText(TenGV)

            for i in range(model.rowCount()):
                self.tableWidget.insertRow(i)
                for j in range(model.columnCount() - 1):
                    data = str(model.record(i).value(Head[j]))
                    self.tableWidget.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    def setupUi(self, GVWindowLD):
        GVWindowLD.setObjectName("GVWindowLD")
        GVWindowLD.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(GVWindowLD)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(20, 80, 1200, 600))
        self.tableWidget.setMouseTracking(True)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(10)
        self.tableWidget.setRowCount(5)
        self.tableWidget.setHorizontalHeaderLabels(('Mã Lơp', 'Mã GV', 'Năm','Kỳ Học','Thứ 2','Thứ 3','Thứ 4','Thứ 5', 'Thứ 6','Mã Môn Học'))

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(500, 700, 100, 60))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.searchData)

        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 26, 150, 40))
        self.label.setObjectName("label")

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(300, 26, 200, 40))
        self.label_4.setObjectName("")


        GVWindowLD.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(GVWindowLD)
        self.statusbar.setObjectName("statusbar")
        GVWindowLD.setStatusBar(self.statusbar)

        self.retranslateUi(GVWindowLD)
        QtCore.QMetaObject.connectSlotsByName(GVWindowLD)

    def retranslateUi(self, GVWindowLD):
        _translate = QtCore.QCoreApplication.translate
        GVWindowLD.setWindowTitle(_translate("GVWindowLD", "GVWindowLD"))
        self.pushButton_4.setText(_translate("GVWindowLD", "Tìm Kiếm"))
        self.label.setText(_translate("GVWindowLD", "Xin chào Giáo Viên: "))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    GVWindowLD = QtWidgets.QMainWindow()
    ui = Ui_GVWindowLD()
    ui.setupUi(GVWindowLD)
    GVWindowLD.show()
    sys.exit(app.exec_())
