# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'GiaoVien.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
import sys
import time
import os

from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
from PyQt5 import QtCore, QtGui, QtWidgets


DRIVER = 'SQL SERVER'
SERVER_NAME = '172.16.71.130'
DB_NAME = 'CSDL_BTN'
UID = 'sa'
PWD = '@xuanhai'

def createconnection():
    conString = f'DRIVER={DRIVER};'\
                f'SERVER={SERVER_NAME};'\
                f'DATABASE={DB_NAME};'\
                f'UID={UID};'\
                f'PWD={PWD}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

            
class InsertDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(InsertDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Register")

        self.setWindowTitle("Add teacher")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.setWindowTitle("Insert teacher Data")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.QBtn.clicked.connect(self.addteacher)

        layout = QVBoxLayout()

        self.MaGVinput = QLineEdit()
        self.MaGVinput.setPlaceholderText("MaGV")
        layout.addWidget(self.MaGVinput)

        self.TenGVinput = QLineEdit()
        self.TenGVinput.setPlaceholderText("TenGV")
        layout.addWidget(self.TenGVinput)
        
        self.NgaySinhinput = QDateEdit()
        layout.addWidget(self.NgaySinhinput)

        self.GioiTinhinput = QLineEdit()
        self.GioiTinhinput.setPlaceholderText("Gioi Tinh")
        layout.addWidget(self.GioiTinhinput)

        self.Emailinput = QLineEdit()
        self.Emailinput.setPlaceholderText("Email")
        layout.addWidget(self.Emailinput)

        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def addteacher(self):
        MaGV = ""
        TenGV = ""
        NgaySinh = ""
        GioiTinh = ""
        Email = ""

        MaGV = self.MaGVinput.text()
        TenGV = self.TenGVinput.text()
        NgaySinh = self.NgaySinhinput.text()
        GioiTinh = self.GioiTinhinput.text()
        Email = self.Emailinput.text()

        if createconnection():
            print('processing query...')
            try:
                qry = QSqlQuery(db)
                qry.prepare("insert into GiaoVien (MaGV, TenGV, NgaySinh, GioiTinh, Email)"
                " VALUES (:MaGV, :TenGV, :NgaySinh, :GioiTinh,:Email)")
                qry.bindValue(0, MaGV)
                qry.bindValue(1, TenGV)
                qry.bindValue(2,NgaySinh)
                qry.bindValue(3,GioiTinh)
                qry.bindValue(8,Email)
                qry.exec()
                QMessageBox.information(QMessageBox(),'Successful','teacher is added successfully to the database.')
                
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Could not add teacher to the database.')



class Ui_AdminTK():

    # def __init__(self, cursor):
    #     self.cursor = cursor

    # print(self.cursor)

    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchteacher()
        if model.rowCount() > 0:
            self.tableView.setRowCount(0)
            Head = ['MaGV','TenGV','NgaySinh', 'GioiTinh','Email']
            for i in range(model.rowCount()):
                print(model.record(i).value('MaGV'))
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = model.record(i).value(Head[j])
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

            
    def InserData(self):
       dlg = InsertDialog()
       dlg.exec_()

    def load(self):
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
 
            qry.prepare("SELECT top 10 * FROM TaiKhoan")
            qry.exec()
            
            model = QSqlTableModel()
            model.setQuery(qry)
            self.tableView.setRowCount(0)
            Head = ['id','username','email', 'image_file','password','permission']
            for i in range(model.rowCount()):
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = model.record(i).value(Head[j])
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    def deletedata(self):
        row = self.tableView.currentRow()
        data = self.tableView.item(row,0).text()
        if createconnection():
            try:
                qry = QSqlQuery(db)
                qry.prepare("DELETE FROM GiaoVien where MaGV = :data")
                qry.bindValue(0, data)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','teacher is deleted successfully to the database.')
                    
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Could not add teacher to the database.')

    def updatedata(self):
        row = self.tableView.currentRow()
        MaGV = self.tableView.item(row,0).text()
        TenGV = self.tableView.item(row,1).text()
        NgaySinh = self.tableView.item(row,2).text()
        GioiTinh = self.tableView.item(row,3).text()
        Email = self.tableView.item(row,4).text()

        if createconnection():     
            try:
                qry = QSqlQuery(db)
                qry.prepare("UPDATE GiaoVien SET TenGV = ?, NgaySinh = ?, GioiTinh = ?, Email =? \
                    WHERE MaGV = ?")
                qry.bindValue(0, TenGV)
                qry.bindValue(1, NgaySinh)
                qry.bindValue(2, GioiTinh)
                qry.bindValue(3, Email)
                qry.bindValue(4, MaGV)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','teacher is updated successfully to the database.')     
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Could not add teacher to the database.')
            db.close()


    def setupUi(self, AdminGV):
        AdminGV.setObjectName("AdminTK")
        AdminGV.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(AdminGV)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView = QtWidgets.QTableWidget(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(0, 0, 1200, 600))
        self.tableView.setRowCount(5)
        self.tableView.setColumnCount(5)
        self.tableView.setHorizontalHeaderLabels(('id', 'Username', 'Email', 'Ảnh chân dung','Mật khẩu', 'Nhóm quyền'))
    
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(50, 700, 80, 40))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.load)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(180, 700, 81, 40))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.InserData)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(320, 700, 81, 40))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.deletedata)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(440, 700, 81, 40))
        self.pushButton_4.setObjectName("pushButton_3")
        self.pushButton_4.clicked.connect(self.updatedata)       

        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(560, 700, 71, 40))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.searchData)


        AdminGV.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(AdminGV)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        AdminGV.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(AdminGV)
        self.statusbar.setObjectName("statusbar")
        AdminGV.setStatusBar(self.statusbar)

        self.retranslateUi(AdminGV)
        QtCore.QMetaObject.connectSlotsByName(AdminGV)

    def retranslateUi(self, AdminGV):
        _translate = QtCore.QCoreApplication.translate
        AdminGV.setWindowTitle(_translate("AdminGV", "AdminGV"))
        self.pushButton.setText(_translate("AdminGV", "Load"))
        self.pushButton_2.setText(_translate("AdminGV", "Insert"))
        self.pushButton_3.setText(_translate("AdminGV", "Delete"))
        self.pushButton_4.setText(_translate("AdminGV", "Update"))
        self.pushButton_5.setText(_translate("AdminGV", "Search"))
 

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AdminGV = QtWidgets.QMainWindow()
    ui = Ui_AdminGV()
    ui.setupUi(AdminGV)
    AdminGV.show()
    sys.exit(app.exec_())
