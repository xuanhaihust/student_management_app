# do not delete below row
# -*- coding: utf-8 -*-

### >>>>>>>>>> Student Management System User Interface >>>>>>>>>> ###
### Run this file to open Main Window ###

from PyQt5 import QtCore, QtGui, QtWidgets
from HocSinh import Ui_AdminWindow
from LopHoc import Ui_AdminLop
from GiaoVien import Ui_AdminGV
from LichDay import Ui_AdminLD
from TaiKhoan import Ui_AdminTK
from LichGiangDay import Ui_GVWindowLD
from BangDiem import Ui_GiaoVienBD
from BTVN import Ui_GiaoVienBTVN

import pyodbc
import hashlib, copy, sys

# Driver={ODBC Driver 13 for SQL Server};Server=tcp:xuanhaihust.database.windows.net,1433;
# Database=STUDENT_DB;Uid=xuanhaihust;Pwd={your_password_here};Encrypt=yes;TrustServerCertificate=no;Connection Timeout=30;

class Connection:
    @classmethod
    def createConnection(cls):
        print("Connecting to SQL Server DB")
        try:
            conn = pyodbc.connect("""Driver={ODBC Driver 17 for SQL Server};
                                Server=tcp:xuanhaihust.database.windows.net,1433;
                                Database=CSDL_BTN;
                                Uid=username;
                                Pwd={password};
                                Encrypt=yes;
                                TrustServerCertificate=no;
                                Connection Timeout=30;""")
                                # 'Trusted_Connection=yes;') # the connection is made pooling by default in pyodbc

            print("Connect successful")
        except:
            print("Fail to connect to SQL Server DB")
            conn = None

        return conn
    @classmethod
    def closeConnection(cls, conn):
        conn.close()

conn = Connection.createConnection()
if conn != None:
    cursor = conn.cursor()


class Ui_MainWindow(object):
    def __init__(self):
        self.login_status = False
        self.permission = 0 # 0: init, None: login fail, 1: admin, 2: giáo viên
        self.active_init = {
            'file': {
                'gioi_thieu': True,
                'dang_nhap': True,
                'dang_xuat': True,
                'close': True
            },
            'admin': {
                'hoc_sinh': False,
                'giao_vien': False,
                'lop_hoc': False,
                'qlld': False,
                'mon_hoc': False,
                'tai_khoan': False
            },
            'giao_vien': {
                'diem_danh': False,
                'btvn': False,
                'bang_diem': False,
                'lich_day': False
            },
            'settings': {
                'resize_window': True,
                'background': True,
                'themes': True
            }
        }
        self.active = copy.deepcopy(self.active_init) # clone a truly independent dict

        self.permission_map = {
            0: 'Guest',
            1: 'Admin',
            2: 'Giáo viên'
        }

    def setupUi(self, MainWindow):

        print("Loading MainWindow")

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(745, 630)
        icon_MainWindow = QtGui.QIcon()
        icon_MainWindow.addPixmap(QtGui.QPixmap("acount.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon_MainWindow)
        MainWindow.setStyleSheet("")
        MainWindow.setIconSize(QtCore.QSize(80, 80))

        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        MainWindow.setCentralWidget(self.centralwidget)

        self.tab_MainWindow = QtWidgets.QTabWidget(self.centralwidget)
        self.tab_MainWindow.setGeometry(QtCore.QRect(0, 0, 745, 571))
        self.tab_MainWindow.setStyleSheet("")
        self.tab_MainWindow.setTabPosition(QtWidgets.QTabWidget.West)
        self.tab_MainWindow.setTabShape(QtWidgets.QTabWidget.Rounded)
        self.tab_MainWindow.setIconSize(QtCore.QSize(80, 100))
        self.tab_MainWindow.setObjectName("tab_MainWindow")
        self.tab_About = QtWidgets.QWidget()
        self.tab_About.setObjectName("tab_About")
        self.text_About = QtWidgets.QTextBrowser(self.tab_About)
        self.text_About.setGeometry(QtCore.QRect(0, 0, 631, 565))
        self.text_About.setObjectName("text_About")

        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("icon/about-us-icon-png-3 - Copy.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tab_MainWindow.addTab(self.tab_About, icon1, "")
        self.tab_Acounts = QtWidgets.QWidget()
        self.tab_Acounts.setObjectName("tab_Acounts")
        self.stack_Acounts = QtWidgets.QStackedWidget(self.tab_Acounts)
        self.stack_Acounts.setGeometry(QtCore.QRect(0, 0, 631, 561))
        self.stack_Acounts.setStyleSheet("")
        self.stack_Acounts.setFrameShape(QtWidgets.QFrame.NoFrame)
        self.stack_Acounts.setFrameShadow(QtWidgets.QFrame.Raised)
        self.stack_Acounts.setObjectName("stack_Acounts")
        self.page_AcountsLogin = QtWidgets.QWidget()
        self.page_AcountsLogin.setObjectName("page_AcountsLogin")
        self.frame_User = QtWidgets.QFrame(self.page_AcountsLogin)
        self.frame_User.setGeometry(QtCore.QRect(100, 130, 441, 80))
        self.frame_User.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.frame_User.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_User.setFrameShadow(QtWidgets.QFrame.Plain)
        self.frame_User.setObjectName("frame_User")
        self.input_User = QtWidgets.QLineEdit(self.frame_User)
        self.input_User.setGeometry(QtCore.QRect(110, 10, 321, 61))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferDefault)
        self.input_User.setFont(font)
        self.input_User.setText("")
        self.input_User.setFrame(False)
        self.input_User.setObjectName("input_User")
        self.label_User = QtWidgets.QLabel(self.frame_User)
        self.label_User.setGeometry(QtCore.QRect(20, 10, 61, 61))
        self.label_User.setText("")
        self.label_User.setTextFormat(QtCore.Qt.AutoText)
        self.label_User.setPixmap(QtGui.QPixmap("icon/people_orig.png"))
        self.label_User.setScaledContents(True)
        self.label_User.setObjectName("label_User")
        self.line_User = QtWidgets.QFrame(self.frame_User)
        self.line_User.setGeometry(QtCore.QRect(90, 10, 3, 61))
        self.line_User.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_User.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_User.setObjectName("line_User")
        self.frame_Password = QtWidgets.QFrame(self.page_AcountsLogin)
        self.frame_Password.setGeometry(QtCore.QRect(100, 220, 441, 80))
        self.frame_Password.setStyleSheet("background-color: rgb(255, 255, 255);")
        self.frame_Password.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_Password.setObjectName("frame_Password")
        self.input_Password = QtWidgets.QLineEdit(self.frame_Password)
        self.input_Password.setGeometry(QtCore.QRect(110, 10, 321, 61))
        font = QtGui.QFont()
        font.setPointSize(14)
        font.setBold(False)
        font.setItalic(False)
        font.setWeight(50)
        font.setKerning(True)
        font.setStyleStrategy(QtGui.QFont.PreferDefault)
        self.input_Password.setFont(font)
        self.input_Password.setText("")
        self.input_Password.setFrame(False)
        self.input_Password.setEchoMode(QtWidgets.QLineEdit.Password)
        self.input_Password.setObjectName("input_Password")
        self.line_Password = QtWidgets.QFrame(self.frame_Password)
        self.line_Password.setGeometry(QtCore.QRect(90, 10, 3, 61))
        self.line_Password.setFrameShape(QtWidgets.QFrame.VLine)
        self.line_Password.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line_Password.setObjectName("line_Password")
        self.label_Password = QtWidgets.QLabel(self.frame_Password)
        self.label_Password.setGeometry(QtCore.QRect(20, 10, 61, 61))
        self.label_Password.setText("")
        self.label_Password.setTextFormat(QtCore.Qt.AutoText)
        self.label_Password.setPixmap(QtGui.QPixmap("icon/UI_Glyph_Blue_1_of_3_32-512 (1).png"))
        self.label_Password.setScaledContents(True)
        self.label_Password.setObjectName("label_Password")
        self.label_Acount = QtWidgets.QLabel(self.page_AcountsLogin)
        self.label_Acount.setGeometry(QtCore.QRect(270, 50, 111, 61))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.label_Acount.setFont(font)
        self.label_Acount.setFrameShadow(QtWidgets.QFrame.Raised)
        self.label_Acount.setLineWidth(0)
        self.label_Acount.setObjectName("label_Acount")
        self.btn_Login = QtWidgets.QPushButton(self.page_AcountsLogin)
        self.btn_Login.setGeometry(QtCore.QRect(220, 330, 211, 71))
        self.btn_Login.setMouseTracking(True)
        self.btn_Login.setAutoFillBackground(False)
        self.btn_Login.setStyleSheet("QPushButton:hover\n"
                                        "{\n"
                                        "   background-color:white;\n"
                                        "}\n"
                                        "")
        self.btn_Login.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("icon/unnamed (1).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_Login.setIcon(icon2)
        self.btn_Login.setIconSize(QtCore.QSize(200, 80))
        self.btn_Login.setFlat(True)
        self.btn_Login.setObjectName("btn_Login")

        self.label_AcountInvalidWarning = QtWidgets.QLabel(self.page_AcountsLogin)
        self.label_AcountInvalidWarning.setGeometry(QtCore.QRect(180, 430, 281, 61))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.label_AcountInvalidWarning.setFont(font)
        self.label_AcountInvalidWarning.setStyleSheet("color: rgb(255, 0, 0);")
        self.label_AcountInvalidWarning.setAlignment(QtCore.Qt.AlignCenter)
        self.label_AcountInvalidWarning.setWordWrap(True)
        self.label_AcountInvalidWarning.setObjectName("label_AcountInvalidWarning")
        self.stack_Acounts.addWidget(self.page_AcountsLogin)
        self.page_LoginSuccessful = QtWidgets.QWidget()
        self.page_LoginSuccessful.setObjectName("page_LoginSuccessful")
        self.label_ImageSuccessfulLogin = QtWidgets.QLabel(self.page_LoginSuccessful)
        self.label_ImageSuccessfulLogin.setGeometry(QtCore.QRect(200, 140, 200, 200))
        self.label_ImageSuccessfulLogin.setText("")
        self.label_ImageSuccessfulLogin.setPixmap(QtGui.QPixmap("icon/—Pngtree—tick vector icon_4070122.png"))
        self.label_ImageSuccessfulLogin.setScaledContents(True)
        self.label_ImageSuccessfulLogin.setObjectName("label_ImageSuccessfulLogin")
        self.label_TextSuccessfullLogin = QtWidgets.QLabel(self.page_LoginSuccessful)
        self.label_TextSuccessfullLogin.setGeometry(QtCore.QRect(220, 390, 171, 31))
        font = QtGui.QFont()
        font.setPointSize(12)
        self.label_TextSuccessfullLogin.setFont(font)
        self.label_TextSuccessfullLogin.setStyleSheet("color: rgb(170, 85, 255);\n"
                                                        "color: rgb(0, 170, 255);")
        self.label_TextSuccessfullLogin.setScaledContents(False)
        self.label_TextSuccessfullLogin.setObjectName("label_TextSuccessfullLogin")
        self.stack_Acounts.addWidget(self.page_LoginSuccessful)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("icon/user_accounts_help - Copy.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tab_MainWindow.addTab(self.tab_Acounts, icon3, "")
        self.tab_Admin = QtWidgets.QWidget()
        self.tab_Admin.setObjectName("tab_Admin")
        self.label_AdminBackground = QtWidgets.QLabel(self.tab_Admin)
        self.label_AdminBackground.setGeometry(QtCore.QRect(-260, 0, 1200, 700))
        self.label_AdminBackground.setText("")
        self.label_AdminBackground.setPixmap(QtGui.QPixmap("icon/0.jpg"))
        self.label_AdminBackground.setScaledContents(True)
        self.label_AdminBackground.setObjectName("label_AdminBackground")
        self.layoutWidget = QtWidgets.QWidget(self.tab_Admin)
        self.layoutWidget.setGeometry(QtCore.QRect(50, 20, 541, 501))
        self.layoutWidget.setObjectName("layoutWidget")
        self.layout_AdminTools = QtWidgets.QGridLayout(self.layoutWidget)
        self.layout_AdminTools.setContentsMargins(0, 0, 0, 0)
        self.layout_AdminTools.setObjectName("layout_AdminTools")
        self.btn_ad_HocSinh = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_ad_HocSinh.setMouseTracking(True)
        self.btn_ad_HocSinh.setAutoFillBackground(False)
        self.btn_ad_HocSinh.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_ad_HocSinh.setText("")
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("icon/57-575127_one-student-cartoon-png-transparent-png.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_ad_HocSinh.setIcon(icon4)
        self.btn_ad_HocSinh.setIconSize(QtCore.QSize(150, 150))
        self.btn_ad_HocSinh.setFlat(True)
        self.btn_ad_HocSinh.setObjectName("btn_ad_HocSinh")
        self.layout_AdminTools.addWidget(self.btn_ad_HocSinh, 0, 0, 1, 1)
        self.btn_ad_GiaoVien = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_ad_GiaoVien.setMouseTracking(True)
        self.btn_ad_GiaoVien.setAutoFillBackground(False)
        self.btn_ad_GiaoVien.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_ad_GiaoVien.setText("")
        icon_ad_GiaoVien = QtGui.QIcon()
        icon_ad_GiaoVien.addPixmap(QtGui.QPixmap("icon/Picture3.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_ad_GiaoVien.setIcon(icon_ad_GiaoVien)
        self.btn_ad_GiaoVien.setIconSize(QtCore.QSize(125, 150))
        self.btn_ad_GiaoVien.setFlat(True)
        self.btn_ad_GiaoVien.setObjectName("btn_ad_GiaoVien")
        self.layout_AdminTools.addWidget(self.btn_ad_GiaoVien, 0, 1, 1, 1)
        self.btn_ad_LopHoc = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_ad_LopHoc.setMouseTracking(True)
        self.btn_ad_LopHoc.setAutoFillBackground(False)
        self.btn_ad_LopHoc.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_ad_LopHoc.setText("")
        icon_ad_LopHoc = QtGui.QIcon()
        icon_ad_LopHoc.addPixmap(QtGui.QPixmap("icon/school-png-icon-1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_ad_LopHoc.setIcon(icon_ad_LopHoc)
        self.btn_ad_LopHoc.setIconSize(QtCore.QSize(130, 150))
        self.btn_ad_LopHoc.setFlat(True)
        self.btn_ad_LopHoc.setObjectName("btn_ad_LopHoc")
        self.layout_AdminTools.addWidget(self.btn_ad_LopHoc, 0, 2, 1, 1)
        self.btn_ad_QuanLyLichDay = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_ad_QuanLyLichDay.setMouseTracking(True)
        self.btn_ad_QuanLyLichDay.setAutoFillBackground(False)
        self.btn_ad_QuanLyLichDay.setStyleSheet("QPushButton:hover\n"
                                                        "{\n"
                                                        "   background-color:white;\n"
                                                        "}\n"
                                                        "")
        self.btn_ad_QuanLyLichDay.setText("")
        icon7 = QtGui.QIcon()
        icon7.addPixmap(QtGui.QPixmap("icon/Apple_Calendar_Icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_ad_QuanLyLichDay.setIcon(icon7)
        self.btn_ad_QuanLyLichDay.setIconSize(QtCore.QSize(120, 120))
        self.btn_ad_QuanLyLichDay.setFlat(True)
        self.btn_ad_QuanLyLichDay.setObjectName("btn_ad_QuanLyLichDay")
        self.layout_AdminTools.addWidget(self.btn_ad_QuanLyLichDay, 1, 0, 1, 1)
        self.btn_ad_MonHoc = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_ad_MonHoc.setMouseTracking(True)
        self.btn_ad_MonHoc.setAutoFillBackground(False)
        self.btn_ad_MonHoc.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_ad_MonHoc.setText("")
        icon8 = QtGui.QIcon()
        icon8.addPixmap(QtGui.QPixmap("icon/AJ_Books.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_ad_MonHoc.setIcon(icon8)
        self.btn_ad_MonHoc.setIconSize(QtCore.QSize(120, 100))
        self.btn_ad_MonHoc.setFlat(True)
        self.btn_ad_MonHoc.setObjectName("btn_ad_MonHoc")
        self.layout_AdminTools.addWidget(self.btn_ad_MonHoc, 1, 1, 1, 1)
        self.btn_ad_TaiKhoan = QtWidgets.QPushButton(self.layoutWidget)
        self.btn_ad_TaiKhoan.setMouseTracking(True)
        self.btn_ad_TaiKhoan.setAutoFillBackground(False)
        self.btn_ad_TaiKhoan.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_ad_TaiKhoan.setText("")
        icon9 = QtGui.QIcon()
        icon9.addPixmap(QtGui.QPixmap("icon/unnamed (4).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_ad_TaiKhoan.setIcon(icon9)
        self.btn_ad_TaiKhoan.setIconSize(QtCore.QSize(120, 120))
        self.btn_ad_TaiKhoan.setFlat(True)
        self.btn_ad_TaiKhoan.setObjectName("btn_ad_TaiKhoan")
        self.layout_AdminTools.addWidget(self.btn_ad_TaiKhoan, 1, 2, 1, 1)
        icon10 = QtGui.QIcon()
        icon10.addPixmap(QtGui.QPixmap("icon/acount - Copy.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tab_MainWindow.addTab(self.tab_Admin, icon10, "")
        self.tab_Teacher = QtWidgets.QWidget()
        self.tab_Teacher.setObjectName("tab_Teacher")
        self.layoutWidget_2 = QtWidgets.QWidget(self.tab_Teacher)
        self.layoutWidget_2.setGeometry(QtCore.QRect(100, 80, 441, 381))
        self.layoutWidget_2.setObjectName("layoutWidget_2")
        self.layout_GiaoVienTools = QtWidgets.QGridLayout(self.layoutWidget_2)
        self.layout_GiaoVienTools.setContentsMargins(0, 0, 0, 0)
        self.layout_GiaoVienTools.setObjectName("layout_GiaoVienTools")
        self.btn_gv_DiemDanh = QtWidgets.QPushButton(self.layoutWidget_2)
        self.btn_gv_DiemDanh.setMouseTracking(True)
        self.btn_gv_DiemDanh.setAutoFillBackground(False)
        self.btn_gv_DiemDanh.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_gv_DiemDanh.setText("")
        icon_gv_DiemDanh = QtGui.QIcon()
        icon_gv_DiemDanh.addPixmap(QtGui.QPixmap("icon/png-for-attendance--256.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_gv_DiemDanh.setIcon(icon_gv_DiemDanh)
        self.btn_gv_DiemDanh.setIconSize(QtCore.QSize(150, 150))
        self.btn_gv_DiemDanh.setFlat(True)
        self.btn_gv_DiemDanh.setObjectName("btn_gv_DiemDanh")
        self.layout_GiaoVienTools.addWidget(self.btn_gv_DiemDanh, 0, 0, 1, 1)
        self.btn_gv_LichDayGiaoVien = QtWidgets.QPushButton(self.layoutWidget_2)
        self.btn_gv_LichDayGiaoVien.setMouseTracking(True)
        self.btn_gv_LichDayGiaoVien.setAutoFillBackground(False)
        self.btn_gv_LichDayGiaoVien.setStyleSheet("QPushButton:hover\n"
                                                        "{\n"
                                                        "   background-color:white;\n"
                                                        "}\n"
                                                        "")
        self.btn_gv_LichDayGiaoVien.setText("")
        icon_gv_LichDayGiaoVien = QtGui.QIcon()
        icon_gv_LichDayGiaoVien.addPixmap(QtGui.QPixmap("icon/unnamed (2).png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_gv_LichDayGiaoVien.setIcon(icon_gv_LichDayGiaoVien)
        self.btn_gv_LichDayGiaoVien.setIconSize(QtCore.QSize(120, 120))
        self.btn_gv_LichDayGiaoVien.setFlat(True)
        self.btn_gv_LichDayGiaoVien.setObjectName("btn_gv_LichDayGiaoVien")
        self.layout_GiaoVienTools.addWidget(self.btn_gv_LichDayGiaoVien, 1, 1, 1, 1)
        self.btn_gv_BTVN = QtWidgets.QPushButton(self.layoutWidget_2)
        self.btn_gv_BTVN.setMouseTracking(True)
        self.btn_gv_BTVN.setAutoFillBackground(False)
        self.btn_gv_BTVN.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_gv_BTVN.setText("")
        icon_gv_BTVN = QtGui.QIcon()
        icon_gv_BTVN.addPixmap(QtGui.QPixmap("icon/gif-education-student-homework-student-png-clip-art.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_gv_BTVN.setIcon(icon_gv_BTVN)
        self.btn_gv_BTVN.setIconSize(QtCore.QSize(120, 150))
        self.btn_gv_BTVN.setFlat(True)
        self.btn_gv_BTVN.setObjectName("btn_gv_BTVN")
        self.layout_GiaoVienTools.addWidget(self.btn_gv_BTVN, 0, 1, 1, 1)
        self.btn_gv_BangDiem = QtWidgets.QPushButton(self.layoutWidget_2)
        self.btn_gv_BangDiem.setMouseTracking(True)
        self.btn_gv_BangDiem.setAutoFillBackground(False)
        self.btn_gv_BangDiem.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_gv_BangDiem.setText("")
        icon_gv_BangDiem = QtGui.QIcon()
        icon_gv_BangDiem.addPixmap(QtGui.QPixmap("icon/png-transparent-school-education-student-college-transcript-good-student-text-material-brand.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_gv_BangDiem.setIcon(icon_gv_BangDiem)
        self.btn_gv_BangDiem.setIconSize(QtCore.QSize(140, 140))
        self.btn_gv_BangDiem.setFlat(True)
        self.btn_gv_BangDiem.setObjectName("btn_gv_BangDiem")
        self.layout_GiaoVienTools.addWidget(self.btn_gv_BangDiem, 1, 0, 1, 1)
        self.label_GiaoVienBackground = QtWidgets.QLabel(self.tab_Teacher)
        self.label_GiaoVienBackground.setGeometry(QtCore.QRect(-260, 0, 1200, 700))
        self.label_GiaoVienBackground.setText("")
        self.label_GiaoVienBackground.setPixmap(QtGui.QPixmap("icon/0.jpg"))
        self.label_GiaoVienBackground.setScaledContents(True)
        self.label_GiaoVienBackground.setObjectName("label_GiaoVienBackground")
        self.label_GiaoVienBackground.raise_()
        self.layoutWidget_2.raise_()
        icon15 = QtGui.QIcon()
        icon15.addPixmap(QtGui.QPixmap("icon/unnamed - Copy.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tab_MainWindow.addTab(self.tab_Teacher, icon15, "")
        self.tab_Settings = QtWidgets.QWidget()
        self.tab_Settings.setObjectName("tab_Settings")
        self.label_SettingsBackground = QtWidgets.QLabel(self.tab_Settings)
        self.label_SettingsBackground.setGeometry(QtCore.QRect(0, -30, 635, 600))
        self.label_SettingsBackground.setText("")
        self.label_SettingsBackground.setPixmap(QtGui.QPixmap("icon/65840343_2282235122105892_2150047034806108160_o.jpg"))
        self.label_SettingsBackground.setScaledContents(True)
        self.label_SettingsBackground.setObjectName("label_SettingsBackground")
        self.gridLayoutWidget = QtWidgets.QWidget(self.tab_Settings)
        self.gridLayoutWidget.setGeometry(QtCore.QRect(50, 410, 531, 161))
        self.gridLayoutWidget.setObjectName("gridLayoutWidget")
        self.gridLayout = QtWidgets.QGridLayout(self.gridLayoutWidget)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.btn_set_ResizeWindow = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btn_set_ResizeWindow.setMouseTracking(True)
        self.btn_set_ResizeWindow.setAutoFillBackground(False)
        self.btn_set_ResizeWindow.setStyleSheet("QPushButton:hover\n"
                                                        "{\n"
                                                        "   background-color:white;\n"
                                                        "}\n"
                                                        "")
        self.btn_set_ResizeWindow.setText("")
        icon16 = QtGui.QIcon()
        icon16.addPixmap(QtGui.QPixmap("icon/Picture1.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_set_ResizeWindow.setIcon(icon16)
        self.btn_set_ResizeWindow.setIconSize(QtCore.QSize(140, 140))
        self.btn_set_ResizeWindow.setFlat(True)
        self.btn_set_ResizeWindow.setObjectName("btn_set_ResizeWindow")
        self.gridLayout.addWidget(self.btn_set_ResizeWindow, 0, 0, 1, 1)
        self.btn_set_Themes = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btn_set_Themes.setMouseTracking(True)
        self.btn_set_Themes.setAutoFillBackground(False)
        self.btn_set_Themes.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_set_Themes.setText("")
        icon17 = QtGui.QIcon()
        icon17.addPixmap(QtGui.QPixmap("icon/d52e022fb1c24b87564f20fa904a4b46_icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_set_Themes.setIcon(icon17)
        self.btn_set_Themes.setIconSize(QtCore.QSize(90, 90))
        self.btn_set_Themes.setFlat(True)
        self.btn_set_Themes.setObjectName("btn_set_Themes")
        self.gridLayout.addWidget(self.btn_set_Themes, 0, 2, 1, 1)
        self.btn_set_Background = QtWidgets.QPushButton(self.gridLayoutWidget)
        self.btn_set_Background.setMouseTracking(True)
        self.btn_set_Background.setAutoFillBackground(False)
        self.btn_set_Background.setStyleSheet("QPushButton:hover\n"
                                                "{\n"
                                                "   background-color:white;\n"
                                                "}\n"
                                                "")
        self.btn_set_Background.setText("")
        icon18 = QtGui.QIcon()
        icon18.addPixmap(QtGui.QPixmap("icon/icon-no-background-icon-512x512-2x-color-shade-fan-png-clip-art.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.btn_set_Background.setIcon(icon18)
        self.btn_set_Background.setIconSize(QtCore.QSize(100, 100))
        self.btn_set_Background.setFlat(True)
        self.btn_set_Background.setObjectName("btn_set_Background")

        self.gridLayout.addWidget(self.btn_set_Background, 0, 1, 1, 1)

        icon19 = QtGui.QIcon()
        icon19.addPixmap(QtGui.QPixmap("icon/apple-settings-1-493162.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.tab_MainWindow.addTab(self.tab_Settings, icon19, "")


        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 745, 18))
        self.menubar.setObjectName("menubar")
        self.menuFile = QtWidgets.QMenu(self.menubar)
        self.menuFile.setObjectName("menuFile")
        self.menuAdmin = QtWidgets.QMenu(self.menubar)
        self.menuAdmin.setObjectName("menuAdmin")
        self.menuGiaoVien = QtWidgets.QMenu(self.menubar)
        self.menuGiaoVien.setObjectName("menuGiaoVien")
        self.menuSettings = QtWidgets.QMenu(self.menubar)
        self.menuSettings.setObjectName("menuSettings")

        MainWindow.setMenuBar(self.menubar)

        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)


        ### >>>>>>>>>>>>>>>>>> MENU >>>>>>>>>>>>>>>>>>>> ###
        # menu File
        self.actionAbout = QtWidgets.QAction(MainWindow)
        self.actionAbout.setObjectName("actionAbout")
        icon_about = QtGui.QIcon()
        icon_about.addPixmap(QtGui.QPixmap("icon/about.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionAbout.setIcon(icon_about)
        self.actionAbout.triggered.connect(self.toPage_About)

        self.actionDangNhap = QtWidgets.QAction(MainWindow)
        self.actionDangNhap.setObjectName("actionDangNhap")
        icon_login = QtGui.QIcon()
        icon_login.addPixmap(QtGui.QPixmap("icon/login.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionDangNhap.setIcon(icon_login)
        self.actionDangNhap.triggered.connect(self.react_actionDangNhap) # navigate to page_AcountsLogin when actionDangNhap is triggered

        self.actionDangXuat = QtWidgets.QAction(MainWindow)
        self.actionDangXuat.setObjectName("actionDangXuat")
        icon_logout = QtGui.QIcon()
        icon_logout.addPixmap(QtGui.QPixmap("icon/logout.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionDangXuat.setIcon(icon_logout)
        self.actionDangXuat.triggered.connect(self.logout) # navigate to page_Login when logout

        self.actionClose = QtWidgets.QAction(MainWindow)
        self.actionClose.setObjectName("action_file_Close")
        icon_close = QtGui.QIcon()
        icon_close.addPixmap(QtGui.QPixmap("icon/close.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionClose.setIcon(icon_close)
        self.actionClose.triggered.connect(self.closeMainWindow)

        
        # menu Admin
        self.actionTaiKhoan = QtWidgets.QAction(MainWindow)
        self.actionTaiKhoan.setIcon(icon9)
        self.actionTaiKhoan.setObjectName("actionTaiKhoan")
        self.actionTaiKhoan.triggered.connect(self.openWindow_Admin_TaiKhoan)
        
        self.actionMonHoc = QtWidgets.QAction(MainWindow)
        self.actionMonHoc.setIcon(icon8)
        self.actionMonHoc.setObjectName("actionMonHoc")

        self.actionHocSinh = QtWidgets.QAction(MainWindow)
        self.actionHocSinh.setObjectName("actionHocSinh")
        icon_ad_HocSinh = QtGui.QIcon()
        icon_ad_HocSinh.addPixmap(QtGui.QPixmap("icon/tina-kurashiki-clip-art-female-teacher.jpg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.actionHocSinh.setIcon(icon_ad_HocSinh)
        self.actionHocSinh.triggered.connect(self.openWindow_Admin_HocSinh)

        self.actionGiaoVien = QtWidgets.QAction(MainWindow)
        self.actionGiaoVien.setObjectName("actionGiaoVien")
        self.actionGiaoVien.setIcon(icon_ad_GiaoVien)
        self.actionGiaoVien.triggered.connect(self.openWindow_Admin_GiaoVien)

        self.actionLopHoc = QtWidgets.QAction(MainWindow)
        self.actionLopHoc.setObjectName("actionLopHoc")
        self.actionLopHoc.setIcon(icon_ad_LopHoc)
        self.actionLopHoc.triggered.connect(self.openWindow_Admin_LopHoc)

        self.actionQuanLyLichDay = QtWidgets.QAction(MainWindow)
        self.actionQuanLyLichDay.setObjectName("actionQuanLichDay")
        self.actionQuanLyLichDay.setIcon(icon7)
        self.actionQuanLyLichDay.triggered.connect(self.openWindow_Admin_QLLD)

        # menu Giáo viên
        self.actionDiemDanh = QtWidgets.QAction(MainWindow)
        self.actionDiemDanh.setObjectName("actionDiemDanh")
        self.actionDiemDanh.setIcon(icon_gv_DiemDanh)
        self.actionDiemDanh.triggered.connect(self.openWindow_GV_DiemDanh)

        self.actionBTVN = QtWidgets.QAction(MainWindow)
        self.actionBTVN.setObjectName("actionBTVN")
        self.actionBTVN.setIcon(icon_gv_BTVN)
        self.actionBTVN.triggered.connect(self.openWindow_GV_BTVN)

        self.actionBangDiem = QtWidgets.QAction(MainWindow)
        self.actionBangDiem.setObjectName("actionBangDiem")
        self.actionBangDiem.setIcon(icon_gv_BangDiem)
        self.actionBangDiem.triggered.connect(self.openWindow_GV_BangDiem)

        self.actionLichDayGiaoVien = QtWidgets.QAction(MainWindow)
        self.actionLichDayGiaoVien.setObjectName("actionGV_lichday")
        self.actionLichDayGiaoVien.setIcon(icon_gv_LichDayGiaoVien)
        self.actionLichDayGiaoVien.triggered.connect(self.openWindow_GV_LichDay)

        # action of menu Settings
        self.actionResize_Window = QtWidgets.QAction(MainWindow)
        self.actionResize_Window.setIcon(icon16)
        self.actionResize_Window.setStatusTip("Resize Window")
        self.actionResize_Window.setObjectName("actionResize_Window")
        
        self.actionBackground_Color = QtWidgets.QAction(MainWindow)
        self.actionBackground_Color.setIcon(icon18)
        self.actionBackground_Color.setObjectName("actionBackground_Color")
        
        self.actionChange_Themes = QtWidgets.QAction(MainWindow)
        self.actionChange_Themes.setIcon(icon17)
        self.actionChange_Themes.setObjectName("actionChange_Themes")


        # add action to menu
        self.menuFile.addAction(self.actionAbout)
        self.menuFile.addAction(self.actionDangNhap)
        self.menuFile.addAction(self.actionDangXuat)
        self.menuFile.addAction(self.actionClose)

        self.menuAdmin.addAction(self.actionHocSinh)
        self.menuAdmin.addAction(self.actionGiaoVien)
        self.menuAdmin.addAction(self.actionLopHoc)
        self.menuAdmin.addAction(self.actionQuanLyLichDay)
        self.menuAdmin.addAction(self.actionMonHoc)
        self.menuAdmin.addAction(self.actionTaiKhoan)
        
        self.menuGiaoVien.addAction(self.actionDiemDanh)
        self.menuGiaoVien.addAction(self.actionBTVN)
        self.menuGiaoVien.addAction(self.actionBangDiem)
        self.menuGiaoVien.addAction(self.actionLichDayGiaoVien)

        self.menuSettings.addAction(self.actionResize_Window)
        self.menuSettings.addAction(self.actionBackground_Color)
        self.menuSettings.addAction(self.actionChange_Themes)

        self.menubar.addAction(self.menuFile.menuAction())
        self.menubar.addAction(self.menuAdmin.menuAction())
        self.menubar.addAction(self.menuGiaoVien.menuAction())
        self.menubar.addAction(self.menuSettings.menuAction())


        ### >>>>>>>>>>>> on-click reaction for buttons in tabs >>>>>>>>>>>>> ###
        # tab TaiKhoan
        self.btn_Login.clicked.connect(self.login)

        # tab Admin
        self.btn_ad_HocSinh.clicked.connect(self.openWindow_Admin_HocSinh)
        self.btn_ad_GiaoVien.clicked.connect(self.openWindow_Admin_GiaoVien)
        self.btn_ad_LopHoc.clicked.connect(self.openWindow_Admin_LopHoc)
        self.btn_ad_QuanLyLichDay.clicked.connect(self.openWindow_Admin_QLLD)
        # self.btn_ad_MonHoc.clicked.connect(self.openWindow_Admin_MonHoc)
        self.btn_ad_TaiKhoan.clicked.connect(self.openWindow_Admin_TaiKhoan)

        # tab GiaoVien
        self.btn_gv_DiemDanh.clicked.connect(self.openWindow_GV_DiemDanh)
        self.btn_gv_BTVN.clicked.connect(self.openWindow_GV_BTVN)
        self.btn_gv_BangDiem.clicked.connect(self.openWindow_GV_BangDiem)
        self.btn_gv_LichDayGiaoVien.clicked.connect(self.openWindow_GV_LichDay)

        # tab Settings
        # self.btn_set_ResizeWindow.clicked.connect(self.do_ResizeWindow)
        # self.btn_set_Background.clicked.connect(self.do_Bakground)
        # self.btn_set_Themes.clicked.connect(self.do_Themes)
        

        # intial MainWindow
        self.retranslateUi(MainWindow)
        self.tab_MainWindow.setCurrentIndex(0) # navigate to tab_About
        QtCore.QMetaObject.connectSlotsByName(MainWindow)


        self.label_AcountInvalidWarning.hide()
        # self.label_AcountInvalidWarning.show()

        print("Main Window is up!")

    ### >>>>>>>>>>>>>>> menu on-click reaction func >>>>>>>>>>>>>> ###
    def closeMainWindow(self):
        MainWindow.close()

    def openWindow_GV_BangDiem(self):
        if self.active['giao_vien']['bang_diem'] == True:
            print("Open window: Bang diem")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_GiaoVienBD()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def openWindow_GV_LichDay(self):
        if self.active['giao_vien']['lich_day'] == True:
            print("Open window: Lich day")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_GVWindowLD()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def openWindow_GV_DiemDanh(self):
        if self.active['giao_vien']['diem_danh'] == True:
            print("Open window: Diem danh")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_GiaovienDD()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))
    
    def openWindow_GV_BTVN(self):
        if self.active['giao_vien']['btvn'] == True:
            print("Open window: BTVN")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_GiaoVienBTVN()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def openWindow_Admin_HocSinh(self):
        if self.active['admin']['hoc_sinh'] == True:
            print("Open window: Hoc sinh")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_AdminWindow()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def openWindow_Admin_LopHoc(self):
        if self.active['admin']['lop_hoc'] == True:
            print("Open window: Lop hoc")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_AdminLop()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def openWindow_Admin_GiaoVien(self):
        if self.active['admin']['giao_vien'] == True:
            print("Open window: Giao vien")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_AdminGV(cursor)
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))
    
    def openWindow_Admin_QLLD(self):
        if self.active['admin']['qlld'] == True:
            print("Open window: Quan ly lich day")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_AdminLD()
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def openWindow_Admin_TaiKhoan(self):
        if self.active['admin']['tai_khoan'] == True:
            print("Open window: Tai Khoan")
            self.window = QtWidgets.QMainWindow()
            self.ui = Ui_AdminTK()
            # self.ui = Ui_AdminTK(cursor)
            self.ui.setupUi(self.window)
            self.window.show()
        else:
            print("Access feature denied for group {}'s user".format(self.permission_map[self.permission]))

    def toPage_AcountsLogin(self):
        self.tab_MainWindow.setCurrentIndex(1) # navigate to tab_Acounts
        self.stack_Acounts.setCurrentIndex(0) # page_AcountsLogin

    def toPage_LoginSuccessful(self):
        self.tab_MainWindow.setCurrentIndex(1) # tab_Acounts
        self.stack_Acounts.setCurrentIndex(1) # page_LoginSuccessful
    
    def toPage_About(self):
        self.tab_MainWindow.setCurrentIndex(0)

    def react_actionDangNhap(self):
        if self.login_status == True:
            self.toPage_LoginSuccessful()
        else:
            self.toPage_AcountsLogin()


    ### >>>>>>>>>>>>>>>> Authentication procedure func >>>>>>>>>>>>> ###
    def permission_mapping(self):
        if self.permission == None or self.permission == 0:
            self.active = copy.deepcopy(self.active_init)
        elif self.permission == 1: # admin
            for key, sub_dict in self.active.items(): # set all True
                for k, val in sub_dict.items():
                    self.active[key][k] = True
        elif self.permission == 2: # Giáo viên
            for key, value in self.active['giao_vien'].items():
                self.active['giao_vien'][key] = True


    def check_user_database(self, username, password):
        """ check the user and password with database
        return json:
            authen = {
                "success": False / True,
                "permission": None / <int>
            }
        """

        cursor.execute("""SELECT tk.permission FROM CSDL_BTN.dbo.TaiKhoan tk WHERE tk.username = '{0}' and tk.password = '{1}';""".format(username, password))

        data = cursor.fetchone()

        if data != None:
            success = True
            permission = data[0]
        else:
            success = False
            permission = None

        authen = {
            "success": success,
            "permission": permission
        }

        return authen

    def hash_str(self, s):
        hash_str = hashlib.md5(s.encode())
        return hash_str.hexdigest()

    def login(self):
        """ do the login precedure when user push btn_Login and self.permission = 0 or None"""
        # take username hay password from input text box
        # hash the password
        # check with database
        # handle case: success / fail
        # return login_status = True/False
        print("Starting login procedure")

        username = self.input_User.text()
        password = self.input_Password.text()

        password = self.hash_str(password)

        try:
            authen = self.check_user_database(username, password)
            if authen['success'] == False:
                self.login_status = False
                self.label_AcountInvalidWarning.show() # if login failed, show warning
                print("Login fail")
            else:
                self.permission = int(authen['permission']) # if success, set the self.permission
                self.login_status = True
                self.toPage_LoginSuccessful()
            
        except:
            self.login_status = False
            print("Something went wrong during access database, try again!")
        
        if self.login_status == True:
            self.permission_mapping()
            print("Login successful, group {} permission assigned".format(self.permission_map[self.permission]))
        

    def logout(self):
        """do the logout procedure when self.permission != 0"""
        # set self.permission = 0, self.login_status (not sign in yet)
        # navigate to page_AcountsLogin
        # clear self.username, self.password of current object
        if self.login_status == True:
            self.toPage_AcountsLogin()
            self.permission = None
            self.login_status = False

            self.permission_mapping()

            print("Logged out")
        else:
            print("User's not login yet")
        

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Quản Lý Học Sinh"))
        self.tab_MainWindow.setStatusTip(_translate("MainWindow", "Tài khoản"))
        self.tab_About.setStatusTip(_translate("MainWindow", "Giới thiệu"))
        self.text_About.setHtml(_translate("MainWindow", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
            "<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
            "p, li { white-space: pre-wrap; }\n"
            "</style></head><body style=\" font-family:\'MS Shell Dlg 2\'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
            "<p align=\"center\" style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-family:\'Times New Roman\'; font-size:18pt; font-weight:696; font-style:italic; color:#000000; background-color:transparent;\">Phần mềm Quản lý học sinh</span><span style=\" font-size:12pt;\"><br /></span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:138%;\"><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Dưới sự hướng dẫn của ThS. Phạm Thị Phương Giang, phần mềm </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; font-weight:696; font-style:italic; color:#000000; background-color:transparent;\">Quản lý học sinh </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">được thực hiện bởi nhóm Covid-9 bao gồm: </span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">🧒  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Đặng Xuân Hải            </span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">👧  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Nguyễn Thị Phương Thảo</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">👧  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Nguyễn Phương Thùy        </span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">👧  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Lê Thị Hoàng Yến</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:138%;\"><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Phần mềm </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; font-weight:696; font-style:italic; color:#000000; background-color:transparent;\">Quản lý học sinh</span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\"> là công cụ phục vụ giáo viên trong công tác quản lý giảng dạy trên lớp cũng như các công tác quản lý dành cho Quản trị viên sẽ được thao tác nhanh chóng và dễ dàng:</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">  </span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:16pt; color:#333333; background-color:#ffffff;\">👨‍✈️</span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">  </span><a name=\"docs-internal-guid-93c4cc33-7fff-28de-828a-1af9e483c258\"></a><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">T</span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">hao tác Quản trị (chỉ dành cho tài khoản Admin)</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">😍</span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:16pt; color:#333333; background-color:#ffffff;\">  </span><a name=\"docs-internal-guid-d25460fb-7fff-367c-c228-6dbe87512562\"></a><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Q</span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">uản lý các thông tin học sinh, giáo viên, lớp học, lịch dạy, môn học</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">😍  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Quản lý tài khoản người dùng trên hệ thống</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">  </span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:16pt; color:#333333; background-color:#ffffff;\">🙎‍♀️</span><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Thao tác Giáo viên</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">😍  </span><a name=\"docs-internal-guid-d52ece86-7fff-c39c-ee5a-238646485e81\"></a><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">C</span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">ung cấp thông tin lịch dạy cho giáo viên</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">😍  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Đưa ra danh sách học sinh trong lớp phục vụ điểm danh</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">😍  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Quản lý bảng điểm của cả lớp cũng như từng học sinh</span></p>\n"
            "<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:1; text-indent:0px; line-height:138%; background-color:transparent;\"><span style=\" font-family:\'Segoe UI Emoji\'; font-size:14pt; color:#333333; background-color:#ffffff;\">😍  </span><span style=\" font-family:\'Times New Roman\'; font-size:12pt; color:#000000; background-color:transparent;\">Quản lý bài tập về nhà của học sinh</span></p></body></html>"))
        self.tab_Acounts.setToolTip(_translate("MainWindow", "Tài khoản"))
        self.tab_Acounts.setStatusTip(_translate("MainWindow", "Tài khoản"))
        self.input_User.setPlaceholderText(_translate("MainWindow", "Username"))
        self.input_Password.setPlaceholderText(_translate("MainWindow", "Password"))
        self.label_Acount.setText(_translate("MainWindow", "Tài khoản"))
        self.btn_Login.setStatusTip(_translate("MainWindow", "Login"))
        self.label_AcountInvalidWarning.setText(_translate("MainWindow", "Thông tin đăng nhập chưa chính xác Vui lòng thử lại!"))
        self.label_TextSuccessfullLogin.setText(_translate("MainWindow", "Đăng nhập thành công!"))
        self.tab_Admin.setStatusTip(_translate("MainWindow", "Quản trị"))
        self.label_AdminBackground.setStatusTip(_translate("MainWindow", "Thao tác quản trị"))
        self.btn_ad_HocSinh.setStatusTip(_translate("MainWindow", "Quản lý học sinh"))
        self.btn_ad_GiaoVien.setStatusTip(_translate("MainWindow", "Quản lý giáo viên"))
        self.btn_ad_LopHoc.setStatusTip(_translate("MainWindow", "Quản lý lớp học"))
        self.btn_ad_QuanLyLichDay.setStatusTip(_translate("MainWindow", "Quản lý lịch dạy"))
        self.btn_ad_MonHoc.setStatusTip(_translate("MainWindow", "Quản lý môn học"))
        self.btn_ad_TaiKhoan.setStatusTip(_translate("MainWindow", "Quản lý tài khoản người dùng"))
        self.tab_Teacher.setStatusTip(_translate("MainWindow", "Giáo viên"))
        self.btn_gv_DiemDanh.setStatusTip(_translate("MainWindow", "Change Background"))
        self.btn_gv_LichDayGiaoVien.setStatusTip(_translate("MainWindow", "Change Background"))
        self.btn_gv_BTVN.setStatusTip(_translate("MainWindow", "Change Background"))
        self.btn_gv_BangDiem.setStatusTip(_translate("MainWindow", "Change Background"))
        self.label_GiaoVienBackground.setStatusTip(_translate("MainWindow", "Thao tác quản trị"))
        self.tab_Settings.setStatusTip(_translate("MainWindow", "Cài đặt"))
        self.label_SettingsBackground.setStatusTip(_translate("MainWindow", "Cài đặt phần mềm"))
        self.btn_set_ResizeWindow.setStatusTip(_translate("MainWindow", "Resize Window"))
        self.btn_set_Themes.setStatusTip(_translate("MainWindow", "Change Themes"))
        self.btn_set_Background.setStatusTip(_translate("MainWindow", "Change Background"))
        self.menuFile.setTitle(_translate("MainWindow", "File"))
        self.menuAdmin.setTitle(_translate("MainWindow", "Admin"))
        self.menuGiaoVien.setTitle(_translate("MainWindow", "Giáo viên"))
        self.menuSettings.setTitle(_translate("MainWindow", "Settings"))
        self.actionDiemDanh.setText(_translate("MainWindow", "Điểm danh"))
        self.actionBTVN.setText(_translate("MainWindow", "Bài tập về nhà"))
        self.actionBangDiem.setText(_translate("MainWindow", "Bảng điểm"))
        self.actionHocSinh.setText(_translate("MainWindow", "Học sinh"))
        self.actionGiaoVien.setText(_translate("MainWindow", "Giáo Viên"))
        self.actionLopHoc.setText(_translate("MainWindow", "Lớp học"))
        self.actionQuanLyLichDay.setText(_translate("MainWindow", "QL Lịch dạy"))
        self.actionMonHoc.setText(_translate("MainWindow", "Môn học"))
        self.actionLichDayGiaoVien.setText(_translate("MainWindow", "Lịch dạy"))
        self.actionClose.setText(_translate("MainWindow", "Đóng"))
        self.actionAbout.setText(_translate("MainWindow", "Giới thiệu"))
        self.actionDangNhap.setText(_translate("MainWindow", "Đăng nhập"))
        self.actionDangXuat.setText(_translate("MainWindow", "Đăng Xuất"))
        self.actionTaiKhoan.setText(_translate("MainWindow", "Tài khoản"))
        self.actionResize_Window.setText(_translate("MainWindow", "Resize Window"))
        self.actionBackground_Color.setText(_translate("MainWindow", "Background Color"))
        self.actionChange_Themes.setText(_translate("MainWindow", "Change Themes"))

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)

    MainWindow.show()
    sys.exit(app.exec_())
