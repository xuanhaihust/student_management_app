# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'HocSinh.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
import sys
import time
import os
from PyQt5 import QtCore, QtGui, QtWidgets

server_name = '172.16.71.130'
database_name = 'CSDL_BTN'
UID = 'sa'
PWD = '@xuanhai'

def createconnection():
    conString = f'DRIVER={{SQL Server}};'\
                f'SERVER={server_name};'\
                f'DATABASE={database_name};'\
                f'UID={UID};'\
                f'PWD={PWD}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

class SearchDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(SearchDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Search")

        self.setWindowTitle("Search user")
        self.setFixedWidth(300)
        self.setFixedHeight(100)
        self.QBtn.clicked.connect(self.searchstudent)
        layout = QVBoxLayout()

        self.searchinput = QLineEdit()
        self.searchinput.setPlaceholderText("Ma HS")
        layout.addWidget(self.searchinput)
        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def searchstudent(self):
        searchrol = ""
        searchrol = self.searchinput.text()
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
            qry.prepare("SELECT * FROM HocSinh where MaHS = :searchrol")
            qry.bindValue(0, searchrol)
            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
            if model.rowCount() > 0:
                QMessageBox.information(QMessageBox(), 'Successful', 'search successful')
                return model
            else:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không tìm thấy kết quả')
                return model
            
class InsertDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(InsertDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Register")

        self.setWindowTitle("Add Student")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.setWindowTitle("Insert Student Data")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.QBtn.clicked.connect(self.addstudent)

        layout = QVBoxLayout()

        self.MaHSinput = QLineEdit()
        self.MaHSinput.setPlaceholderText("MaHS")
        layout.addWidget(self.MaHSinput)

        self.TenHSinput = QLineEdit()
        self.TenHSinput.setPlaceholderText("TenHS")
        layout.addWidget(self.TenHSinput)
        
        self.NgaySinhinput = QDateEdit()

        layout.addWidget(self.NgaySinhinput)

        self.GioiTinhinput = QLineEdit()
        self.GioiTinhinput.setPlaceholderText("Gioi Tinh")
        layout.addWidget(self.GioiTinhinput)

        self.PhuHuynhinput = QLineEdit()
        self.PhuHuynhinput.setPlaceholderText("Phu Huynh")
        layout.addWidget(self.PhuHuynhinput)

        self.DiaChiinput = QLineEdit()
        self.DiaChiinput.setPlaceholderText("Dia Chi")
        layout.addWidget(self.DiaChiinput)

        self.SDTinput = QLineEdit()
        self.SDTinput.setPlaceholderText("SDT Phu Huynh")
        layout.addWidget(self.SDTinput)

        self.MaLopinput = QLineEdit()
        self.MaLopinput.setPlaceholderText("Ma Lop")
        layout.addWidget(self.MaLopinput)

        self.Emailinput = QLineEdit()
        self.Emailinput.setPlaceholderText("Email")
        layout.addWidget(self.Emailinput)

        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def addstudent(self):
        MaHS = ""
        TenHS = ""
        NgaySinh = ""
        GioiTinh = ""
        PhuHuynh = ""
        DiaChi = ""
        SDT_PH  =""
        MaLop = ""
        Email = ""

        MaHS = self.MaHSinput.text()
        TenHS = self.TenHSinput.text()
        NgaySinh = self.NgaySinhinput.text()
        GioiTinh = self.GioiTinhinput.text()
        PhuHuynh = self.PhuHuynhinput.text()
        DiaChi = self.DiaChiinput.text()
        SDT_PH  = self.SDTinput.text()
        MaLop = self.MaLopinput.text()
        Email = self.Emailinput.text()

        if createconnection():
            print('processing query...')
            try:
                qry = QSqlQuery(db)
                qry.prepare("insert into Hocsinh (MaHS, TenHS, NgaySinh, GioiTinh, PhuHuynh, DiaChi,  SDT_PH, MaLop, Email)"
                " VALUES (:MaHS, :TenHS, :NgaySinh, :GioiTinh, :PhuHuynh, :DiaChi, :SDT_PH, :MaLop, :Email)")
                qry.bindValue(0, MaHS)
                qry.bindValue(1, TenHS)
                qry.bindValue(2, NgaySinh)
                qry.bindValue(3, GioiTinh)
                qry.bindValue(4, PhuHuynh)
                qry.bindValue(5, DiaChi)
                qry.bindValue(6, SDT_PH)
                qry.bindValue(7,MaLop)
                qry.bindValue(8,Email)
                qry.exec()
                QMessageBox.information(QMessageBox(),'Successful','Student is added successfully to the database.')
                

            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Could not add student to the database.')



class Ui_AdminWindow(object):
    # def openWindow(self):
    #     self.window = QtWidgets.QMainWindow()
    #     self.ui = Ui_InsertHSWindow()
    #     self.ui.setupUi(self.window)
    #     self.window.show()

    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchstudent()
        if model.rowCount() > 0:
            self.tableView.setRowCount(0)
            Head = ['MaHS','TenHS','NgaySinh', 'GioiTinh', 'PhuHuynh','DiaChi','SDT_PH','MaLop','Email']
            for i in range(model.rowCount()):
                print(model.record(i).value('MaHS'))
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = model.record(i).value(Head[j])
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))
            
    def InserData(self):
       dlg = InsertDialog()
       dlg.exec_()

    def load(self):
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
 
            qry.prepare("SELECT top 10 * FROM HocSinh")
            qry.exec()
            
            model = QSqlTableModel()
            model.setQuery(qry)
            self.tableView.setRowCount(0)
            Head = ['MaHS','TenHS','NgaySinh', 'GioiTinh', 'PhuHuynh','DiaChi','SDT_PH','MaLop','Email']
            for i in range(model.rowCount()):
                print(model.record(i).value('MaHS'))
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = model.record(i).value(Head[j])
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    def deletedata(self):
        row = self.tableView.currentRow()
        data = self.tableView.item(row,0).text()
        if createconnection():
            try:
                qry = QSqlQuery(db)
                qry.prepare("DELETE FROM HocSinh where MaHS = :data")
                qry.bindValue(0, data)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','Học Sinh đã được thêm vào cở sở dữ liệu')
                    
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không thể thêm học sinh')

    def updatedata(self):
        row = self.tableView.currentRow()
        MaHS = self.tableView.item(row,0).text()
        TenHS = self.tableView.item(row,1).text()
        NgaySinh = self.tableView.item(row,2).text()
        GioiTinh = self.tableView.item(row,3).text()
        PhuHuynh = self.tableView.item(row,4).text()
        DiaChi = self.tableView.item(row,5).text()
        SDT_PH  = self.tableView.item(row,6).text()
        MaLop = self.tableView.item(row,7).text()
        Email = self.tableView.item(row,8).text()

        if createconnection():     
            try:
                qry = QSqlQuery(db)
                qry.prepare("UPDATE HocSinh SET TenHS = ?, NgaySinh = ?, GioiTinh = ?,PhuHuynh = ?, DiaChi =?, SDT_PH=?, MaLop=?, Email =? \
                    WHERE MaHS = ?")
                qry.bindValue(0, TenHS)
                qry.bindValue(1, NgaySinh)
                qry.bindValue(2, GioiTinh)
                qry.bindValue(3, PhuHuynh)
                qry.bindValue(4, DiaChi)
                qry.bindValue(5, SDT_PH)
                qry.bindValue(6, MaLop)
                qry.bindValue(7, Email)
                qry.bindValue(8, MaHS)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','Student is updated successfully to the database.')     
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Could not add student to the database.')
            db.close()


    def setupUi(self, AdminWindow):
        AdminWindow.setObjectName("AdminWindow")
        AdminWindow.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(AdminWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView = QtWidgets.QTableWidget(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(0, 0, 1200, 600))
        self.tableView.setRowCount(5)
        self.tableView.setColumnCount(9)
        self.tableView.setHorizontalHeaderLabels(('MaHS', 'Tên HS', 'Ngày Sinh', 'Giới Tính', 'Phụ Huynh', 'Địa Chỉ','SDT phụ huynh', 'Mã Lớp','Email'))
    
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(50, 700, 80, 40))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.load)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(180, 700, 81, 40))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.InserData)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(320, 700, 81, 40))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.deletedata)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(440, 700, 81, 40))
        self.pushButton_4.setObjectName("pushButton_3")
        self.pushButton_4.clicked.connect(self.updatedata)       

        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(560, 700, 71, 40))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.searchData)


        AdminWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(AdminWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        AdminWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(AdminWindow)
        self.statusbar.setObjectName("statusbar")
        AdminWindow.setStatusBar(self.statusbar)

        self.retranslateUi(AdminWindow)
        QtCore.QMetaObject.connectSlotsByName(AdminWindow)

    def retranslateUi(self, AdminWindow):
        _translate = QtCore.QCoreApplication.translate
        AdminWindow.setWindowTitle(_translate("AdminWindow", "AdminWindow"))
        self.pushButton.setText(_translate("AdminWindow", "Load"))
        self.pushButton_2.setText(_translate("AdminWindow", "Insert"))
        self.pushButton_3.setText(_translate("AdminWindow", "Delete"))
        self.pushButton_4.setText(_translate("AdminWindow", "Update"))
        self.pushButton_5.setText(_translate("AdminWindow", "Search"))
 

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AdminWindow = QtWidgets.QMainWindow()
    ui = Ui_AdminWindow()
    ui.setupUi(AdminWindow)
    AdminWindow.show()
    sys.exit(app.exec_())
