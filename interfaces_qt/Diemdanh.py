# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DiemDanh.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
import sys
import time
import os
from PyQt5 import QtCore, QtGui, QtWidgets

DRIVER = 'SQL SERVER'
SERVER_NAME = '172.16.71.130'
DB_NAME = 'CSDL_BTN'
UID = 'sa'
PWD = '@xuanhai'

def createconnection():
    conString = f'DRIVER={DRIVER};'\
                f'SERVER={SERVER_NAME};'\
                f'DATABASE={DB_NAME};'\
                f'UID={UID};'\
                f'PWD={PWD}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

class SearchDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(SearchDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Search")

        self.setWindowTitle("Search class")
        self.setFixedWidth(300)
        self.setFixedHeight(300)
        self.QBtn.clicked.connect(self.searchclass)
        layout = QVBoxLayout()

        self.searchMaGVinput = QLineEdit()
        self.searchMaGVinput.setPlaceholderText("Ma Giáo Viên")

        self.searchMaLopinput = QLineEdit()
        self.searchMaLopinput.setPlaceholderText("Mã Lớp")

        layout.addWidget(self.searchMaLopinput)
        layout.addWidget(self.searchMaGVinput)
        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def searchclass(self):
        MaGV = ""
        MaGV = self.searchMaGVinput.text()
        MaLop = self.searchMaLopinput.text()

        if createconnection():
            try:
                qry = QSqlQuery(db)
                qry.prepare("SELECT DiemDanh.MaGV, MaLop, DiemDanh.MaHS, TenHS, DiHoc, NghiCoPhep, NghiKhongPhep, TenGV from \
                            DiemDanh inner join HocSinh on DiemDanh.MaHS = HocSinh.MaHS \
                            inner join GiaoVien on DiemDanh.MaGV = GiaoVien.MaGV\
                            Where DiemDanh.MaGV = ? and MaLop = ?")
                            
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaLop)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                if model.rowCount() > 0:
                    QMessageBox.information(QMessageBox(), 'Successful', 'search successful')
                    return model
                else:
                    QMessageBox.warning(QMessageBox(), 'Error', 'Could not Find class from the database.')
                    return model
            except Exception:
                    QMessageBox.warning(QMessageBox(), 'Error', 'Could not Find class from the database.')
                    return model
            db.close()


class Ui_GiaovienDD(object):

    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchclass()
        if model.rowCount() > 0:
            self.tableWidget.setRowCount(0)
            Head = ['MaGV', 'MaLop', 'MaHS', 'TenHS', 'DiHoc', 'NghiCoPhep', 'NghiKhongPhep']

            TenGV = model.record(0).value('TenGV')
            MaLop = model.record(0).value('MaLop')
            count = model.record(0).value('DiHoc') + model.record(0).value('NghiCoPhep') +model.record(0).value('NghiKhongPhep') +1

            self.label_4.setText(TenGV)
            self.label_5.setText(MaLop)
            self.label_6.setText(str(count))

            for i in range(model.rowCount()):
                self.tableWidget.insertRow(i)
                for j in range(model.columnCount()):
                    if j == 7:
                        diemdanh = QtWidgets.QComboBox()
                        diemdanh.addItem("DiHoc")
                        diemdanh.addItem("NghiCoPhep")
                        diemdanh.addItem("NghiKhongPhep")
                        self.tableWidget.setCellWidget(i,j, diemdanh)
                    else:
                        data = str(model.record(i).value(Head[j]))
                        self.tableWidget.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    
    def savedata(self):
        row = self.tableWidget.currentRow()
        MaGV = self.tableWidget.item(row,0).text()
        MaHS = self.tableWidget.item(row,2).text()
        DiemDanh = str(self.tableWidget.cellWidget(row,7).currentText())
        print(DiemDanh)

        if DiemDanh == 'DiHoc':
            if createconnection():     
                qry = QSqlQuery(db)
                qry.prepare("UPDATE DiemDanh SET DiHoc = DiHoc + 1 \
                        WHERE MaGV = ? and MaHS = ?")       
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaHS)
                qry.exec()     
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')
                data = int(self.tableWidget.item(row,4).text()) + 1
                self.tableWidget.setItem(row,4,QtWidgets.QTableWidgetItem(str(data)))
        elif DiemDanh == 'NghiCoPhep':
            if createconnection():     
                qry = QSqlQuery(db)
                qry.prepare("UPDATE DiemDanh SET NghiCoPhep = NghiCoPhep + 1 \
                        WHERE MaGV = ? and MaHS = ?")       
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaHS)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                # QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')     
                data = int(self.tableWidget.item(row,5).text()) + 1
                self.tableWidget.setItem(row,5,QtWidgets.QTableWidgetItem(str(data)))
                if data >=3:
                    HS = self.tableWidget.item(row,3).text()
                    QMessageBox.warning(QMessageBox(),'warning','Học Sinh {HS} đã nghỉ quá 3 buổi học hãy liên hệ với GVCN')
                else:
                    QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')
        else:
            if createconnection():     
                qry = QSqlQuery(db)
                qry.prepare("UPDATE DiemDanh SET NghiKhongPhep = NghiKhongPhep + 1 \
                        WHERE MaGV = ? and MaHS = ?")       
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaHS)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                data = int(self.tableWidget.item(row,6).text()) + 1
                self.tableWidget.setItem(row,6,QtWidgets.QTableWidgetItem(str(data)))
                if data >=3:
                    HS = self.tableWidget.item(row,3).text()
                    QMessageBox.warning(QMessageBox(),'warning','Học Sinh'+ str(HS) + 'đã nghỉ quá 3 buổi học hãy liên hệ với GVCN')
                else:
                    QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')


    def backdata(self):
        row = self.tableWidget.currentRow()
        MaGV = self.tableWidget.item(row,0).text()
        MaHS = self.tableWidget.item(row,2).text()
        DiemDanh = str(self.tableWidget.cellWidget(row,7).currentText())
        print(DiemDanh)

        if DiemDanh == 'DiHoc':
            if createconnection():     
                qry = QSqlQuery(db)
                qry.prepare("UPDATE DiemDanh SET DiHoc = DiHoc - 1 \
                        WHERE MaGV = ? and MaHS = ?")       
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaHS)
                qry.exec()     
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')
                data = int(self.tableWidget.item(row,4).text()) - 1
                self.tableWidget.setItem(row,4,QtWidgets.QTableWidgetItem(str(data)))
        elif DiemDanh == 'NghiCoPhep':
            if createconnection():     
                qry = QSqlQuery(db)

                qry.prepare("UPDATE DiemDanh SET NghiCoPhep = NghiCoPhep - 1 \
                        WHERE MaGV = ? and MaHS = ?")       
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaHS)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')     
                data = int(self.tableWidget.item(row,5).text()) - 1
                self.tableWidget.setItem(row,5,QtWidgets.QTableWidgetItem(str(data)))
        else:
            if createconnection():     
                qry = QSqlQuery(db)
                qry.prepare("UPDATE DiemDanh SET NghiKhongPhep = NghiKhongPhep - 1 \
                        WHERE MaGV = ? and MaHS = ?")       
                qry.bindValue(0, MaGV)
                qry.bindValue(1, MaHS)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','updated successfully to the database.')
                data = int(self.tableWidget.item(row,6).text()) - 1
                self.tableWidget.setItem(row,6,QtWidgets.QTableWidgetItem(str(data)))
   

    def setupUi(self, GiaovienDD):
        GiaovienDD.setObjectName("GiaovienDD")
        GiaovienDD.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(GiaovienDD)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 60, 1200, 600))
        self.tableWidget.setMouseTracking(True)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(8)
        self.tableWidget.setRowCount(5)
        self.tableWidget.setHorizontalHeaderLabels(('MaGV', 'Lớp', 'MaHS','Tên HS','Đi Học','Nghỉ có phép','Nghỉ Không Phép','Điểm danh'))

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(50, 700, 80, 40))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.backdata)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(200, 700, 80, 40))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.savedata)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(350, 700, 80, 40))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.searchData)


        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 26, 80, 21))
        self.label.setObjectName("label")

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(150, 26, 200, 21))
        self.label_4.setObjectName("")

        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(400, 26, 40, 20))
        self.label_2.setObjectName("label_2")

        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(450, 26, 40, 20))
        self.label_5.setObjectName("")

        self.label_3 = QtWidgets.QLabel(self.centralwidget)
        self.label_3.setGeometry(QtCore.QRect(600, 30, 60, 20))
        self.label_3.setObjectName("label_3")

        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(680, 30, 60, 20))
        self.label_6.setObjectName("label_3")


        GiaovienDD.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(GiaovienDD)
        self.statusbar.setObjectName("statusbar")
        GiaovienDD.setStatusBar(self.statusbar)

        self.retranslateUi(GiaovienDD)
        QtCore.QMetaObject.connectSlotsByName(GiaovienDD)

    def retranslateUi(self, GiaovienDD):
        _translate = QtCore.QCoreApplication.translate
        GiaovienDD.setWindowTitle(_translate("GiaovienDD", "GiaovienDD"))
        self.pushButton_2.setText(_translate("GiaovienDD", "Quay Lại"))
        self.pushButton_3.setText(_translate("GiaovienDD", "Lưu"))
        self.pushButton_4.setText(_translate("GiaovienDD", "Tìm Kiếm"))
        self.label.setText(_translate("GiaovienDD", "Giáo Viên: "))
        self.label_2.setText(_translate("GiaovienDD", "Lớp: "))
        self.label_3.setText(_translate("GiaovienDD", "Buổi học: "))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    GiaovienDD = QtWidgets.QMainWindow()
    ui = Ui_GiaovienDD()
    ui.setupUi(GiaovienDD)
    GiaovienDD.show()
    sys.exit(app.exec_())
