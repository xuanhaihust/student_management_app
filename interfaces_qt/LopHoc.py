# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'HocSinh.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
import sys
import time
import os
from PyQt5 import QtCore, QtGui, QtWidgets

DRIVER = 'SQL SERVER'
SERVER_NAME = '172.16.71.130'
DB_NAME = 'CSDL_BTN'
UID = 'sa'
PWD = '@xuanhai'

def createconnection():
    conString = f'DRIVER={DRIVER};'\
                f'SERVER={SERVER_NAME};'\
                f'DATABASE={DB_NAME};'\
                f'UID={UID};'\
                f'PWD={PWD}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

class SearchDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(SearchDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Search")

        self.setWindowTitle("Search user")
        self.setFixedWidth(300)
        self.setFixedHeight(100)
        self.QBtn.clicked.connect(self.searchstudent)
        layout = QVBoxLayout()

        self.searchinput = QLineEdit()
        self.searchinput.setPlaceholderText("Ma Lớp")
        layout.addWidget(self.searchinput)
        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def searchstudent(self):
        searchrol = ""
        searchrol = self.searchinput.text()
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
            qry.prepare("SELECT * FROM LopHoc where MaLop = ?")
            qry.bindValue(0, searchrol)
            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
            if model.rowCount() > 0:
                QMessageBox.information(QMessageBox(), 'Successful', 'search successful')
                return model
            else:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không tìm thấy kết quả')
                return model

            
class InsertDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(InsertDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Register")

        self.setWindowTitle("Add Class")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.setWindowTitle("Insert Class")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.QBtn.clicked.connect(self.addclass)

        layout = QVBoxLayout()

        self.MaLopinput = QLineEdit()
        self.MaLopinput.setPlaceholderText("MaLop")
        layout.addWidget(self.MaLopinput)

        self.GVCNinput = QLineEdit()
        self.GVCNinput.setPlaceholderText("GVCN")
        layout.addWidget(self.GVCNinput)

        self.PhongHocinput = QLineEdit()
        self.PhongHocinput.setPlaceholderText("Phòng Học")
        layout.addWidget(self.PhongHocinput)

        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def addclass(self):
        MaLop = ""
        GVCN = ""
        PhongHoc = ""

        MaLop = self.MaLopin.text()
        GVCN= self.GVCNinput.text()

        PhongHoc = self.PhongHocinput.text()


        if createconnection():
            print('processing query...')
            try:
                qry = QSqlQuery(db)
                qry.prepare("insert into LopHoc (MaLop, GVCN, PhongHoc)"
                " VALUES (:MaLop, :GVCN, :PhongHoc)")
                qry.bindValue(0, MaLop)
                qry.bindValue(1, GVCN)
                qry.bindValue(2,PhongHoc)
                qry.exec()
                QMessageBox.information(QMessageBox(),'Successful','Lớp học được thêm thành công')
            
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không thể thêm lớp học')
        db.close()



class Ui_AdminLop(object):


    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchstudent()
        if model.rowCount() >0:
            self.tableView.setRowCount(0)
            Head = ['MaLop','GVCN','PhongHoc']
            for i in range(model.rowCount()):
                print(model.record(i).value('MaLop'))
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = model.record(i).value(Head[j])
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

            db.close()
            
    def InserData(self):
       dlg = InsertDialog()
       dlg.exec_()

    def load(self):
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
            qry.prepare("SELECT top 10 * from LopHoc")
            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
            self.tableView.setRowCount(0)
            Head = ['MaLop','GVCN','PhongHoc']
            for i in range(model.rowCount()):
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = model.record(i).value(Head[j])
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    def deletedata(self):
        row = self.tableView.currentRow()
        data = self.tableView.item(row,0).text()
        if createconnection():
            try:
                qry = QSqlQuery(db)
                qry.prepare("DELETE FROM LopHoc where MaLop = :data")
                qry.bindValue(0, data)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','Xóa thành công')
                    
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không thể xóa dữ liệu')

    def updatedata(self):
        row = self.tableView.currentRow()
        MaLop = self.tableView.item(row,0).text()
        GVCN = self.tableView.item(row,1).text()
        PhongHoc = self.tableView.item(row,2).text()

        if createconnection():     
            try:
                qry = QSqlQuery(db)
                qry.prepare("UPDATE LopHoc SET GVCN = ?, PhongHoc = ? \
                        WHERE MaLop = ?")
                qry.bindValue(0, GVCN)
                qry.bindValue(1, PhongHoc)
                qry.bindValue(2, MaLop)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','Lớp được thêm thành công')     
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không thể tìm thấy kết quả')
            db.close()


    def setupUi(self, AdminLop):
        AdminLop.setObjectName("AdminLop")
        AdminLop.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(AdminLop)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView = QtWidgets.QTableWidget(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(0, 0, 1200, 600))
        self.tableView.setRowCount(5)
        self.tableView.setColumnCount(3)
        self.tableView.setHorizontalHeaderLabels(('MaLop', 'GVCN', 'Phòng Học'))
    
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(50, 700, 80, 40))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.load)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(180, 700, 81, 40))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.InserData)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(320, 700, 81, 40))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.deletedata)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(440, 700, 81, 40))
        self.pushButton_4.setObjectName("pushButton_3")
        self.pushButton_4.clicked.connect(self.updatedata)       

        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(560, 700, 71, 40))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.searchData)


        AdminLop.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(AdminLop)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        AdminLop.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(AdminLop)
        self.statusbar.setObjectName("statusbar")
        AdminLop.setStatusBar(self.statusbar)

        self.retranslateUi(AdminLop)
        QtCore.QMetaObject.connectSlotsByName(AdminLop)

    def retranslateUi(self, AdminLop):
        _translate = QtCore.QCoreApplication.translate
        AdminLop.setWindowTitle(_translate("AdminLop", "Lớp Học"))
        self.pushButton.setText(_translate("AdminLop", "Load"))
        self.pushButton_2.setText(_translate("AdminLop", "Thêm"))
        self.pushButton_3.setText(_translate("AdminLop", "Xóa"))
        self.pushButton_4.setText(_translate("AdminLop", "Cập nhật"))
        self.pushButton_5.setText(_translate("AdminLop", "Tìm kiếm"))
 

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AdminLop = QtWidgets.QMainWindow()
    ui = Ui_AdminLop()
    ui.setupUi(AdminLop)
    AdminLop.show()
    sys.exit(app.exec_())
