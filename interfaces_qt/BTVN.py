# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'DiemDanh.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
import sys
import time
import os
from PyQt5 import QtCore, QtGui, QtWidgets

server_name = 'LAPTOP-ENF51THV\SQLEXPRESS'
database_name = 'CSDL_BTN'

def createconnection():
    conString = f'DRIVER={{SQL Server}};'\
                f'SERVER={server_name};'\
                f'DATABASE={database_name}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

class SearchDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(SearchDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Search")

        self.setWindowTitle("Search class")
        self.setFixedWidth(300)
        self.setFixedHeight(300)
        self.QBtn.clicked.connect(self.searchclass)
        layout = QVBoxLayout()

        self.searchMaMHinput = QLineEdit()
        self.searchMaMHinput.setPlaceholderText("Ma Môn Học")

        self.searchMaLopinput = QLineEdit()
        self.searchMaLopinput.setPlaceholderText("Mã Lớp")

        layout.addWidget(self.searchMaLopinput)
        layout.addWidget(self.searchMaMHinput)
        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def searchclass(self):

        MaMH = self.searchMaMHinput.text()
        MaLop = self.searchMaLopinput.text()

        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
            qry.prepare("SELECT BTVN.*, TenMH , MaLop, TenHS from \
                        BTVN inner join MonHoc on BTVN.MaMH = MonHoc.MaMH\
                        inner join HocSinh on BTVN.MaHS = HocSinh.MaHS \
                         Where BTVN.MaMH = ? and MaLop = ?")
                        
            qry.bindValue(0, MaMH)
            qry.bindValue(1, MaLop)
            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
            if model.rowCount() > 0:
                QMessageBox.information(QMessageBox(), 'Successful', 'search successful')
                return model
            else:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không Tìm Thấy kết quả')
                return model
                
            db.close()


class Ui_GiaoVienBTVN(object):

    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchclass()

        self.tableWidget.setRowCount(0)
        Head = ['MaMH', 'MaLop', 'MaHS', 'TenHS', 'NamHoc', 'BaiTap1', 'NgayNop_BT1','Dien_BT1','BaiTap2', 'NgayNop_BT2','Dien_BT2']

        TenMH = model.record(0).value('TenMH')
        MaLop = model.record(0).value('MaLop')

        self.label_4.setText(TenMH)
        self.label_5.setText(MaLop)
        for i in range(model.rowCount()):
            self.tableWidget.insertRow(i)
            for j in range(model.columnCount()-1):
                data = str(model.record(i).value(Head[j]))
                self.tableWidget.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    
    def savedata(self):
        row = self.tableWidget.currentRow()
        MaMH = self.tableWidget.item(row,0).text()
        MaHS = self.tableWidget.item(row,2).text()
        BaiTap1 = self.tableWidget.item(row,5).text()
        NgayNop_BT1 = self.tableWidget.item(row,6).text()
        Dien_BT1 = float(self.tableWidget.item(row,7).text())
        BaiTap2 = self.tableWidget.item(row,8).text()
        NgayNop_BT2 = self.tableWidget.item(row,9).text()
        Dien_BT2 = float(self.tableWidget.item(row,10).text())

        if createconnection():     
            qry = QSqlQuery(db)

            qry.prepare("UPDATE BTVN set BaiTap1 = ?, NgayNop_BT1 = ?, Dien_BT1 =?, BaiTap2 =? , NgayNop_BT2 =?, Dien_BT2 = ?\
                        WHERE MaMH = ? and MaHS = ?")  
            qry.bindValue(0, BaiTap1) 
            qry.bindValue(1,NgayNop_BT1)
            qry.bindValue(2,Dien_BT1)
            qry.bindValue(3,BaiTap2)
            qry.bindValue(4,NgayNop_BT2)
            qry.bindValue(5,Dien_BT2)    
            qry.bindValue(6, MaMH)
            qry.bindValue(7, MaHS)
            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
        db.close()


    def setupUi(self, GiaoVienBTVN):
        GiaoVienBTVN.setObjectName("GiaoVienBTVN")
        GiaoVienBTVN.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(GiaoVienBTVN)
        self.centralwidget.setObjectName("centralwidget")
        self.tableWidget = QtWidgets.QTableWidget(self.centralwidget)
        self.tableWidget.setGeometry(QtCore.QRect(10, 60, 1200, 600))
        self.tableWidget.setMouseTracking(True)
        self.tableWidget.setObjectName("tableWidget")
        self.tableWidget.setColumnCount(11)
        self.tableWidget.setRowCount(5)
        self.tableWidget.setHorizontalHeaderLabels(('Mã Môn Học', 'Lớp', 'Mã HS','Tên HS','Năm Học', 'Bài tập 1','Ngày nộp BT1', 'Điểm BT1', \
                                                    'Bài Tập 2','Ngày Nộp BT2','Điểm BT2'))


        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(200, 700, 80, 40))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.savedata)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(350, 700, 80, 40))
        self.pushButton_4.setObjectName("pushButton_4")
        self.pushButton_4.clicked.connect(self.searchData)


        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(70, 26, 80, 21))
        self.label.setObjectName("label")

        self.label_4 = QtWidgets.QLabel(self.centralwidget)
        self.label_4.setGeometry(QtCore.QRect(150, 26, 200, 21))
        self.label_4.setObjectName("")

        self.label_2 = QtWidgets.QLabel(self.centralwidget)
        self.label_2.setGeometry(QtCore.QRect(400, 26, 40, 20))
        self.label_2.setObjectName("label_2")

        self.label_5 = QtWidgets.QLabel(self.centralwidget)
        self.label_5.setGeometry(QtCore.QRect(450, 26, 40, 20))
        self.label_5.setObjectName("")

        self.label_6 = QtWidgets.QLabel(self.centralwidget)
        self.label_6.setGeometry(QtCore.QRect(700, 700,250 , 20))
        self.label_6.setObjectName("Định dạng ngày: YYYY-MM-DD")




        GiaoVienBTVN.setCentralWidget(self.centralwidget)
        self.statusbar = QtWidgets.QStatusBar(GiaoVienBTVN)
        self.statusbar.setObjectName("statusbar")
        GiaoVienBTVN.setStatusBar(self.statusbar)

        self.retranslateUi(GiaoVienBTVN)
        QtCore.QMetaObject.connectSlotsByName(GiaoVienBTVN)

    def retranslateUi(self, GiaoVienBTVN):
        _translate = QtCore.QCoreApplication.translate
        GiaoVienBTVN.setWindowTitle(_translate("GiaoVienBTVN", "GiaoVienBTVN"))
        self.pushButton_3.setText(_translate("GiaoVienBTVN", "Lưu"))
        self.pushButton_4.setText(_translate("GiaoVienBTVN", "Tìm Kiếm"))
        self.label.setText(_translate("GiaoVienBTVN", "Môn Học: "))
        self.label_2.setText(_translate("GiaoVienBTVN", "Lớp: "))
        self.label_6.setText("Định dạng ngày: YYYY-MM-DD")



if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    GiaoVienBTVN = QtWidgets.QMainWindow()
    ui = Ui_GiaoVienBTVN()
    ui.setupUi(GiaoVienBTVN)
    GiaoVienBTVN.show()
    sys.exit(app.exec_())
