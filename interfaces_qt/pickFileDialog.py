import sys
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *

class FileDialog(QWidget):
    def __init__(self):
        super().__init__()
            
        layout = QVBoxLayout()
        self.btn = QPushButton("QFileDialog static method demo")
        self.btn.clicked.connect(self.openFileNameDialog)
            
        layout.addWidget(self.btn)
        self.le = QLabel("Hello")
            
        layout.addWidget(self.le)
        self.btn1 = QPushButton("QFileDialog object")
        self.btn1.clicked.connect(self.saveFileDialog)
        layout.addWidget(self.btn1)
            
        self.contents = QTextEdit()
        layout.addWidget(self.contents)
        self.setLayout(layout)
        self.setWindowTitle("File Dialog demo")

    def openFileNameDialog(self):
        # options = QFileDialog.Options()
        # options = QFileDialog.DontUseNativeDialog # do not use current OS dialog
        # fileName, _ = QFileDialog.getOpenFileName(self,"Open File", "E:\\","All Files (*);;Image files (*.jpg *.gif *.png)", options=options)
         # return file dir and pick method (All Files), ;; to separate new line each file type
        fileDir, _ = QFileDialog.getOpenFileName(self,"Open File", "E:\\","All Files (*);;Excel files (*.xlsx *.csv *.xml)")
        if fileDir:
            print(fileDir)
        
        return fileDir

    def saveFileDialog(self):
        # options = QFileDialog.Options()
        # options = QFileDialog.DontUseNativeDialog # do not use current OS dialog
        # fileName, _ = QFileDialog.getOpenFileName(self,"Open File", "E:\\","All Files (*);;Image files (*.jpg *.gif *.png)", options=options)
        # return a tuple of file dir and pick method (All Files)
        fileDir, _ = QFileDialog.getSaveFileName(self,"Open File", "E:\\","All Files (*);;Image files (*.jpg *.gif *.png)")
        if fileDir:
            print(fileDir)

        return fileDir
    
            
    def getfiles(self):
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setFilter("Text files (*.txt)")
        filenames = QStringList()
            
        if dlg.exec_():
            filenames = dlg.selectedFiles()
            f = open(filenames[0], 'r')
                
            with f:
                data = f.read()
                self.contents.setText(data)
				
def main():
    app = QApplication(sys.argv)
    ex = FileDialog()
    ex.show()
    sys.exit(app.exec_())
        
if __name__ == '__main__':
    main()