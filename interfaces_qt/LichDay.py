# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'QuanLyLichDay.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!
from PyQt5.QtSql import QSqlDatabase, QSqlQueryModel, QSqlQuery, QSqlTableModel
from PyQt5.QtWidgets import QTableView, QApplication
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtPrintSupport import *
import sys
import time
import os
from PyQt5 import QtCore, QtGui, QtWidgets

DRIVER = 'SQL SERVER'
SERVER_NAME = '172.16.71.130'
DB_NAME = 'CSDL_BTN'
UID = 'sa'
PWD = '@xuanhai'

def createconnection():
    conString = f'DRIVER={DRIVER};'\
                f'SERVER={SERVER_NAME};'\
                f'DATABASE={DB_NAME};'\
                f'UID={UID};'\
                f'PWD={PWD}'
    global db
    db = QSqlDatabase.addDatabase('QODBC')
    db.setDatabaseName(conString)
    if db.open():
        print('ok')
        return True
    else:
        print('Fail')
        return False

class SearchDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(SearchDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Search")

        self.setWindowTitle("Search user")
        self.setFixedWidth(300)
        self.setFixedHeight(100)
        self.QBtn.clicked.connect(self.searchLichday)
        layout = QVBoxLayout()

        self.searchinput = QLineEdit()

        self.searchinput.setPlaceholderText("Ma GV")
        layout.addWidget(self.searchinput)
        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def searchLichday(self):
        searchrol = ""
        searchrol = self.searchinput.text()
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
            qry.prepare("SELECT * FROM QuanLyLichDay where MaGV = :searchrol")
            qry.bindValue(0, searchrol)
            qry.exec()
            model = QSqlTableModel()
            model.setQuery(qry)
            if model.rowCount() > 0:
                QMessageBox.information(QMessageBox(), 'Successful', 'search successful')
                return model
            else:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không tìm thấy kết quả')
                return model
  
            
class InsertDialog(QDialog):
    def __init__(self, *args, **kwargs):
        super(InsertDialog, self).__init__(*args, **kwargs)

        self.QBtn = QPushButton()
        self.QBtn.setText("Register")

        self.setWindowTitle("Add Lichday")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.setWindowTitle("Insert Lichday Data")
        self.setFixedWidth(500)
        self.setFixedHeight(500)

        self.QBtn.clicked.connect(self.addLichday)

        layout = QVBoxLayout()

        self.MaLopinput = QLineEdit()
        self.MaLopinput.setPlaceholderText("MaLop")
        layout.addWidget(self.MaLopinput)

        self.MaGVinput = QLineEdit()
        self.MaGVinput.setPlaceholderText("MaGV")
        layout.addWidget(self.MaGVinput)
        
        self.NamHocinput = QLineEdit()
        self.NamHocinput.setPlaceholderText("Năm Học")
        layout.addWidget(self.NamHocinput)

        self.KyHocinput = QLineEdit()
        self.KyHocinput.setPlaceholderText("Kỳ Học")
        layout.addWidget(self.KyHocinput)

        self.Mondayinput = QLineEdit()
        self.Mondayinput.setPlaceholderText("Monday")
        layout.addWidget(self.Mondayinput)

        self.Tuesdayinput = QLineEdit()
        self.Tuesdayinput.setPlaceholderText("Tuesday")
        layout.addWidget(self.Tuesdayinput)

        self.Wednesdayinput = QLineEdit()
        self.Wednesdayinput.setPlaceholderText("Wednesday")
        layout.addWidget(self.Wednesdayinput)

        self.Thursdayinput = QLineEdit()
        self.Thursdayinput.setPlaceholderText("Thursday")
        layout.addWidget(self.Thursdayinput)

        self.Fridayinput = QLineEdit()
        self.Fridayinput.setPlaceholderText("Friday")
        layout.addWidget(self.Fridayinput)

        self.MaMHinput = QLineEdit()
        self.MaMHinput.setPlaceholderText("Mã Môn")
        layout.addWidget(self.MaMHinput)

        layout.addWidget(self.QBtn)
        self.setLayout(layout)

    def addLichday(self):
        MaLop = ""
        MaGV = ""
        NamHoc = ""
        KyHoc = ""
        Monday = ""
        Tuesday = ""
        Wednesday  =""
        Thursday = ""
        Friday = ""
        MaMH = ""

        MaLop = self.MaLopinput.text()
        MaGV = self.MaGVinput.text()
        NamHoc = int(self.NamHocinput.text())
        KyHoc = int(self.KyHocinput.text())
        Monday = self.Mondayinput.text()
        Tuesday = self.Tuesdayinput.text()
        Wednesday  = self.SDTinput.text()
        Thursday = self.Thursdayinput.text()
        Friday = self.Fridayinput.text()
        MaMH = self.MaMHinput.text()

        if createconnection():
            print('processing query...')
            try:
                qry = QSqlQuery(db)
                qry.prepare("insert into QuanLyLichDay (MaLop, MaGV, NamHoc, KyHoc, Monday, Tuesday,  Wednesday, Thursday, Friday, MaMH)"
                " VALUES (:MaLop, :MaGV, :NamHoc, :KyHoc, :Monday, :Tuesday, :Wednesday, :Thursday, :Friday :MaMH)")
                qry.bindValue(0, MaLop)
                qry.bindValue(1, MaGV)
                qry.bindValue(2,NamHoc)
                qry.bindValue(3,KyHoc)
                qry.bindValue(4, Monday)
                qry.bindValue(5, Tuesday)
                qry.bindValue(6, Wednesday)
                qry.bindValue(7,Thursday)
                qry.bindValue(8,Friday)
                qry.bindValue(9,MaMH)
                qry.exec()
                QMessageBox.information(QMessageBox(),'Successful','Thêm thành công')
                
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không thể thêm lịch dạy')



class Ui_AdminLD(object):
    def searchData(self):
        dlg = SearchDialog()
        dlg.exec_()
        model = dlg.searchLichday()
        if model.rowCount() > 0:
            self.tableView.setRowCount(0)
            Head = ['MaLop','MaGV','NamHoc', 'KyHoc', 'Monday','Tuesday','Wednesday','Thursday','Friday', 'MaMH']
            for i in range(model.rowCount()):
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = str(model.record(i).value(Head[j]))
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

            
    def InserData(self):
       dlg = InsertDialog()
       dlg.exec_()

    def load(self):
        if createconnection():
            print('processing query...')
            qry = QSqlQuery(db)
 
            qry.prepare("SELECT top 10 * FROM QuanLyLichDay")
            qry.exec()
            
            model = QSqlTableModel()
            model.setQuery(qry)
            self.tableView.setRowCount(0)
            Head = ['MaLop','MaGV','NamHoc', 'KyHoc', 'Monday','Tuesday','Wednesday','Thursday','Friday', 'MaMH']
            for i in range(model.rowCount()):
                self.tableView.insertRow(i)
                for j in range(model.columnCount()):
                    data = str(model.record(i).value(Head[j]))
                    self.tableView.setItem(i,j,QtWidgets.QTableWidgetItem(data))

    def deletedata(self):
        row = self.tableView.currentRow()
        MaLop = self.tableView.item(row,0).text()
        MaGV = self.tableView.item(row,1).text()
        if createconnection():
            try:
                qry = QSqlQuery(db)
                qry.prepare("DELETE FROM QuanLyLichDay where MaLop = ? and MaGV = ? ")
                qry.bindValue(0, MaLop)
                qry.bindValue(1, MaGV)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','Xóa thành công')
                    
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Không tìm thấy kết quả')

    def updatedata(self):
        row = self.tableView.currentRow()
        MaLop = self.tableView.item(row,0).text()
        MaGV = self.tableView.item(row,1).text()
        NamHoc = int(self.tableView.item(row,2).text())
        KyHoc = int(self.tableView.item(row,3).text())
        Monday = self.tableView.item(row,4).text()
        Tuesday = self.tableView.item(row,5).text()
        Wednesday  = self.tableView.item(row,6).text()
        Thursday = self.tableView.item(row,7).text()
        Friday = self.tableView.item(row,8).text()
        MaMH = self.tableView.item(row,9).text()

        if createconnection():     
            try:
                qry = QSqlQuery(db)
                qry.prepare("UPDATE QuanLyLichDay SET NamHoc = ?, KyHoc = ?,Monday = ?, Tuesday =?, Wednesday=?, Thursday=?, Friday =?, MaMH =? \
                    WHERE MaLop = ? and MaGV = ?")
                
                qry.bindValue(0, NamHoc)
                qry.bindValue(1, KyHoc)
                qry.bindValue(2, Monday)
                qry.bindValue(3, Tuesday)
                qry.bindValue(4, Wednesday)
                qry.bindValue(5, Thursday)
                qry.bindValue(6, Friday)
                qry.bindValue(7, MaMH)
                qry.bindValue(8, MaLop)
                qry.bindValue(9, MaGV)
                qry.exec()
                model = QSqlTableModel()
                model.setQuery(qry)
                QMessageBox.information(QMessageBox(),'Successful','Lichday is updated successfully to the database.')     
            except Exception:
                QMessageBox.warning(QMessageBox(), 'Error', 'Could not add Lichday to the database.')
            db.close()


    def setupUi(self, AdminLD):
        AdminLD.setObjectName("AdminLD")
        AdminLD.resize(1200, 800)
        self.centralwidget = QtWidgets.QWidget(AdminLD)
        self.centralwidget.setObjectName("centralwidget")
        self.tableView = QtWidgets.QTableWidget(self.centralwidget)
        self.tableView.setGeometry(QtCore.QRect(0, 0, 1200, 600))
        self.tableView.setRowCount(5)
        self.tableView.setColumnCount(10)
        self.tableView.setHorizontalHeaderLabels(('Mã Lớp', 'Tên GV', 'Năm Học', 'Kỳ Học', 'Monday', 'Tuesday','Wednesday', 'Thursday','Friday', 'Mã Môn'))
    
        self.pushButton = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton.setGeometry(QtCore.QRect(50, 700, 80, 40))
        self.pushButton.setObjectName("pushButton")
        self.pushButton.clicked.connect(self.load)

        self.pushButton_2 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_2.setGeometry(QtCore.QRect(180, 700, 81, 40))
        self.pushButton_2.setObjectName("pushButton_2")
        self.pushButton_2.clicked.connect(self.InserData)

        self.pushButton_3 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_3.setGeometry(QtCore.QRect(320, 700, 81, 40))
        self.pushButton_3.setObjectName("pushButton_3")
        self.pushButton_3.clicked.connect(self.deletedata)

        self.pushButton_4 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_4.setGeometry(QtCore.QRect(440, 700, 81, 40))
        self.pushButton_4.setObjectName("pushButton_3")
        self.pushButton_4.clicked.connect(self.updatedata)       

        self.pushButton_5 = QtWidgets.QPushButton(self.centralwidget)
        self.pushButton_5.setGeometry(QtCore.QRect(560, 700, 71, 40))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_5.clicked.connect(self.searchData)


        AdminLD.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(AdminLD)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 18))
        self.menubar.setObjectName("menubar")
        AdminLD.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(AdminLD)
        self.statusbar.setObjectName("statusbar")
        AdminLD.setStatusBar(self.statusbar)

        self.retranslateUi(AdminLD)
        QtCore.QMetaObject.connectSlotsByName(AdminLD)

    def retranslateUi(self, AdminLD):
        _translate = QtCore.QCoreApplication.translate
        AdminLD.setWindowTitle(_translate("AdminLD", " Quản lý lịch giảng dạy"))
        self.pushButton.setText(_translate("AdminLD", "Load"))
        self.pushButton_2.setText(_translate("AdminLD", "Thêm"))
        self.pushButton_3.setText(_translate("AdminLD", "Xóa"))
        self.pushButton_4.setText(_translate("AdminLD", "Cập nhật"))
        self.pushButton_5.setText(_translate("AdminLD", "Tìm kiếm"))
 

if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    AdminLD = QtWidgets.QMainWindow()
    ui = Ui_AdminLD()
    ui.setupUi(AdminLD)
    AdminLD.show()
    sys.exit(app.exec_())
