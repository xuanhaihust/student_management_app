# Student Management Application

This is an application for student management, provides features for teacher, student can access and involve their learning, teaching workflow with an user friendly UI. It also provides Administrator account who can manipulate users and archive data.
I built this application as a project in DBMS subject at HUST.

folders:
 - Slide_BaiGiang: slides and lectures of this subject
 - dbms_project: our subject project which is about building a Student Management System on SQL Server with PyQt front-end interfaces.
    + docs and slide: our slides and report
    + interface_qt: source code of interfaces (python and PyQt5)
    + sql_scripts: .sql file for re-produce database
